import theme from '@nuxt/content-theme-docs'

export default theme({
  docs: {
    primaryColor: '#67A2E0'
  },
  content: {
    liveEdit: false
  },
  router: {
    base: '/app/'
  },
  telemetry: false
})
