---
title: Playlists
description: Playlist management
position: 9
category: Endpoints
---

## Get list of playlists

<endpoint command="getPlaylists" front="restricted" auth user="guest">
<template #description>

Query list of playlists.

Guests or users will not see invisible playlists.

</template>
<template #success-response>

```js[Response]
[
    {
        plaid: 1,
        name: 'My Playlist',
        karacount: 49,                  // number of songs in playlist
        duration: 3600,                 // duration in seconds
        time_left: 1200,                // time left before playlist ends (based on song with flag_playing's position,
        created_at: '2021-01-02 13:30'  // Date object
        modified_at: '2021-01-02 13:30' // Date object
        flag_visible: true,
        flag_current: true,
        flag_public: true,
        plcontent_id_playing: 12        // Playlist content which has the flag_playing
        username: 'Axel'                // Who created the playlist
    },
    ...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_LIST_ERROR'
    }
}
```

</template>
</endpoint>

## Create playlist

<endpoint command="createPlaylist" front="closed" auth user="admin">
<template #description>
Create a new playlist
</template>
<template #request>

```js[Request]
{
    name: 'My new playlist',
    flag_visible: true,
    flag_current: false,
    flag_public: false,
    flag_smart: false,
    flag_whitelist: false,
    flag_blacklist: false
}
```

</template>
<template #success-response>

```js[Response]
{
	plaid: 2
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_CREATE_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get single playlist

<endpoint command="getPlaylist" front="restricted" auth user="guest">
<template #description>
Get a single playlist
</template>
<template #request>

```js[Request]
{
    plaid: 2
}
```

</template>
<template #success-response>

See [getPlaylists](#get-playlists)'s response. You receive a single object instead of an array.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_VIEW_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Playlist not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'PL_VIEW_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Edit playlist

<endpoint command="editPlaylist" front="closed" auth user="admin">
<template #description>
Edit playlist's information
</template>
<template #request>

```js[Request]
{
    plaid: 2,
    name: 'My new renamed playlist',
    flag_visible: true,
    flag_current: true,
    flag_smart: true,
    smart_limit_order: 'newest', // or 'oldest'
    smart_limit_number: 100,
    smart_limit_type: 'songs', // or 'duration'
    flag_smartlimit: true,
    type_smart: 'UNION' // Or 'INTERSECT' for AND playlists
}
```

Only `plaid` is mandatory. Any omitted setting isn't modified.

Note on `flag_public` and `flag_current` : Setting these to `false` has no effect. You need to set another playlist as public or current first, as there can be only one playlist with these flags.

</template>
<template #success-response>

Simply returns.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_UPDATE_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Playlist not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'PL_UPDATE_ERROR'		
    }
}
```
</base-code-block>
</template>
</endpoint>

## Delete playlist

<endpoint command="deletePlaylist" front="closed" auth user="admin">
<template #description>
Delete a playlist and its contents.
</template>
<template #request>

```js[Request]
{
  plaid: 2
}
```

</template>
<template #success-response>

Simply resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_DELETE_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Playlist not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'PL_DELETE_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Playlist cannot be deleted">

Usually happens if playlist is either current or public.

```js[Response]
{
    code: 409,
    message: {
        code: 'PL_DELETE_ERROR',
        msg: 'Playlist 1 is current. Unable to delete it. Make another playlist current first.'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Empty playlist

<endpoint command="emptyPlaylist" front="closed" auth user="admin">
<template #description>
Empty a playlist of all its songs
</template>
<template #request>

```js[Request]
{
  plaid: 2
}
```

</template>
<template #success-response>

Simply resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_EMPTY_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Find playing song's index

<endpoint command="findPlayingSongInPlaylist" front="closed" auth user="admin">
<template #description>
Find the currently playing song's index in a playlist. This helps when you do not load the full playlist and need to find out where the current song is to jump to it.
</template>
<template #request>

```js[Request]
{
  plaid: 2
}
```

</template>
<template #success-response>

```js[Response]
{
	index: 203
}
```

Note: `index` is -1 if flag_playing isn't set on any song in the playlist.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500    
}
```

</base-code-block>
</template>
</endpoint>

## Get playlist contents

<endpoint command="getPlaylistContents" front="restricted" auth user="guest">
<template #description>
Returns an array of playlist contents. 
</template>
<template #request>

```js[Request]
{
  plaid: 2,
  filter: null,
  from: 0,            // -1 if you want to get to the currently playing song's position
  size: 200,
  random: null        // Return X random songs from the playlist
  orderByLikes: false // Order results by number of likes/accepted/refused status
}
```

`orderByLikes` is useful for the suggestion playlist. It returns non-accepted/non-refused songs first, then accepted ones, then refused ones. It orders each group by likes in descending order.

</template>
<template #success-response>

Extends [the Get Songs response](karas#get-songs) with additional data : 

```js[Response]
{
    infos: <see Get songs>
    contents: [
        {
            <see Get Songs>,
            criterias: [
                {
                    type: 7
                    value: <TID>
                }
            ], // Criterias list criterias used to get this song in the smart playlist (if we're listing a smart playlist's contents)
            created_at: <Date object>, // When the song was added to the playlist
            nickname: 'Axel-kun'       // Different from username, this is the nickname used when the song was added
            username: 'axel@mykmrepo.net',
            avatar_file: 'abcdefgh.png',
            user_type: 'admin',
            pos: 4,                    // Position in playlist,
            playlistcontent_id: 39,
            flag_playing: true,        // Song is currently playing
            upvotes: 0,                // Number of upvotes,
            flag_visible: true,        // Song is visible in playlist to regular users/guests,
            flag_free: true,           // Song has been freed and doesn't count in a user's quota
            flag_refused: false,       // Song hasn't been refused by the operator yet
            flag_accepted: false,      // Song hasn't been accepted by the operator yet        
        },
        ...
    ]
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_VIEW_SONGS_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Playlist not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'PL_VIEW_SONGS_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Get playlist contents (micro)

<endpoint command="getPlaylistContentsMicro" front="restricted" auth user="guest">
<template #description>

This version only returns a small subset of information from a playlist.

</template>
<template #request>

```js[Request]
{
  plaid: '<uuid>',
}
```

</template>
<template #success-response>

```js[Response]
[
    {
        kid: <uuid>,
        username: axel@mykmrepo.net, // Requester
        plcid: 20938,
        flag_playing: false,
        pos: 5,
        plaid: <uuid>,
        nickname: 'Axel Terizaki',
        flag_free: false,
        flag_visible: true, 
        flag_accepted: false,
        flag_refused: false,
        mediafile: 'FRE - Mahoromatic - AMV - Derniere Danse.mp4',
        repository: 'mykmrepo.net',
        mediasize: 12319290,
        duration: 240
    }, ...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_VIEW_SONG_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Add song to playlist

<endpoint command="addKaraToPlaylist" front="open" auth user="guest">
<template #description>
Add one or several songs to a playlist at a given position.

Some checks are disabled when an admin uses this function.

</template>
<template #request>

```js[Request]
{
  plaid: '<uuid>',
  requester: 'Axel Terizaki' // Optional
  kids: [         // KIDs go here
    'bbbbbbbbb',
    'aaaaaaaaa',   
  ]
  pos: 2          // Optional, see below.
}
```

`pos` is optional. 

- If undefined, songs are added at the end of the playlist. 
- If specified, songs are added at `pos`.
- If `pos` is set to -1, songs are added after the currently playing song.

</template>
<template #success-response>

```js[Response]
{
    plc: ...
}
```

`plc` contains the first song added's full [Playlist content information](#get-single-playlist-content). See response for more details. You can use notably `plc.time_before_play` to have an idea of when the sogn you added is going to play.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_ADD_SONG_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,
    message: {
        // Validation errors here
    }
}
```

</base-code-block>
</template>
</endpoint>

## Randomize a PLC

<endpoint command="randomizePLC" front="closed" auth user="admin">
<template #description>
Randomizes one or more songs in a playlist
</template>
<template #request>

```js[Request]
{
  plc_ids: [239, 240, 289]
}
```

</template>
<template #success-response>

Simply resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500    
}
```

</base-code-block>

</endpoint>

## Copy song to another playlist

<endpoint command="copyKaraToPlaylist" front="closed" auth user="admin">
<template #description>
Copies a song from one playlist to another.
</template>
<template #request>

```js[Request]
{
  plaid: 5, // Destination playlist
  plc_ids: [ // Source PLCs to copy
    53,
    13,
    22
  ]
  pos: 2
}
```

See how `pos` is treated in [addKaraToPlaylist above](#add-song-to-playlist)

</template>
<template #success-response>

Simply resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_SONG_COPY_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,
    message: {
        // Validation error here
    }
}
```

</base-code-block>
</template>
</endpoint>

## Remove song from playlist

<endpoint command="deleteKaraFromPlaylist" front="closed" auth user="admin">
<template #description>
Removes a song from a playlist
</template>
<template #request>

```js[Request]
{
  plc_ids: [ 
    53,
    13,
    22
  ]  
}
```

</template>
<template #success-response>

Simply resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_SONG_DELETE_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,
    message: {
        // Validation error here
    }
}
```

</base-code-block>
</template>
</endpoint>

## Get single playlist content

<endpoint command="getPLC" front="restricted" auth user="guest">
<template #description>
Retrieves a single song from a playlist. This contains additional information than the [get playlist contents](#get-playlist-contents) command.
</template>
<template #request>

```js[Request]
{
  plc_id: 12
}
```

</template>
<template #success-response>

Extends the response from the [get playlist contents](#get-playlist-contents) command.

```js[Response]
{
    <See get playlist contents>
    kara_created_at: <Date>,  // When the song was added to the repository
    kara_modified_at: <Date>, // When the song was last updated in the repository
    time_before_play: 134     // seconds before the song is due to play.
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_VIEW_CONTENT_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Edit playlist content

<endpoint command="editPLC" front="closed" auth user="admin">
<template #description>
Edit various information about a song in a playlist
</template>
<template #request>

You can specify only the items you want to change

```js[Request]
{
  plc_ids: [
      12,
      53
  ],
  pos: 5,              // Update song's position
  flag_playing: true,  // This song is now the one about to be played,
  flag_free: true,
  flag_visible: true,
  flag_accepted: true, // Accept this song
  flag_refused: false
}
```

</template>
<template #success-response>

Simply resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_MODIFY_CONTENT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,
    message: {
        // Validation errors here
    }
}
```

</base-code-block>
</template>
</endpoint>

## Up/downvote a song

<endpoint command="votePLC" front="restricted" auth user="guest">
<template #description>
Upvote or downvote a song in a playlist
</template>
<template #request>

```js[Request]
{
  plc_id: 2
  downvote: true // optional
}
```

</template>
<template #success-response>

Simply resolves.

</template>
<template #errors>

Replace `UPVOTE` by `DOWNVOTE` in the following error message when applicable.

<base-code-block label="PLC unknown">

```js[Response]
{
    code: 404,    
}
```

</base-code-block>
<base-code-block label="Playlist is not the public">

```js[Response]
{
    code: 403,
    message: {
        code: 'UPVOTE_FAILED
    }
}
```

</base-code-block>
<base-code-block label="Not your own PLC">

You cannot vote for a song you requested.

```js[Response]
{
    code: 403,
    message: {
        code: 'UPVOTE_NO_SELF
    }
}
```

</base-code-block>
<base-code-block label="Already upvoted">

```js[Response]
{
    code: 403,
    message: {
        code: 'UPVOTE_ALREADY_DONE
    }
}
```

</base-code-block>
</template>
</endpoint>

## Export playlist

<endpoint command="exportPlaylist" front="closed" auth user="admin">
<template #description>
Export a playlist
</template>
<template #request>

```js[Request]
{
    plaid: 2
}
```

</template>
<template #success-response>

```js[Response]
{
    "Header": {
        "description": "Karaoke Mugen Playlist File",
        "version": 4
    },
    "PlaylistContents": [
        {
            "flag_playing": true,
            "nickname": "lol",
            "created_at": <Date>,
            "pos": 1,
            "username": "axel@mykmrepo.net",
            "flag_free": false,
            "flag_visible": false,
            "flag_accepted": false,
            "flag_refused": false,
            "kid": "b0de301c-5756-49fb-b019-85a99a66586b"
        },
        ...
    ],
    "PlaylistInformation": {
        "created_at": <Date>
        "flag_visible": true,
        "modified_at": <Date>
        "name": "Test",
        "time_left": 0
    } 
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
    message: {
        code: 'PL_EXPORT_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Import playlist

<endpoint command="importPlaylist" front="closed" auth user="admin">
<template #description>
Import a playlist from JSON data
</template>
<template #request>

```js[Request]
{
    playlist: ...
}
```

For `playlist` data, see [export playlist above](#export-playlist).

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'PL_IMPORTED',
        data: {
            plaid: 5,  // newly created playlist ID
            unknownKaras: [
                'aaaaa'
            ]                // Array of KIDs unknown to the local database but available on online repositories. Array length 0 if all songs are in local database.
        }
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
    message: {
        code: 'PL_IMPORT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,    
    message: {
        // Validation error here
    }
}
```

</base-code-block>
</template>
</endpoint>

## Shuffle playlist

<endpoint command="shufflePlaylist" front="closed" auth user="admin">
<template #description>
Shuffle a playlist with a specific method
</template>
<template #request>

```js[Request]
{
    plaid: 2,
    method: 'normal',
    fullShuffle: false  // Shuffle entire playlist instead of only after currently playing song
}
```

`method` can be : 

- `normal` : Random shuffle
- `smart` : Shuffle but avoids long songs close to each other, or one person having all its songs bundled together
- `balance` : Creates pools of users and balances songs among the pools
- `upvotes` : Sorts the playlist by number of upvotes

</template>
<template #success-response>

Simply resolves

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
    message: {
        code: 'PL_SHUFFLE_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Create Automix

<endpoint command="createAutomix" front="closed" auth user="admin">
<template #description>

Create an automix playlist with a set of criterias. This is similar to smart playlists but the playlsit created is static and can be modified manually.

</template>
<template #request>

```js[Request]
{
    filters: {
        usersFavorites: [
            axel@mykmrepo.net,
            leonekmi@mykmrepo.net
        ],
        usersAnimeList?: [
            axel@mykmrepo.net
        ],
        years: [1982, 2003],
        tags: [
            {
                tid: '...',
                type: 8
            }
        ]
    },
    limitType: 'duration', // "songs" is another option
    limitNumber: 3600, // seconds for duration, or number of songs for songs limit type
    playlistName: 'My shiny new automix',
    surprisePlaylist: false // if true then all songs added will be hidden	
}
```

</template>
<template #success-response>

```js[Response]
{
    plaid: 5 // newly created playlist
	playlist_name: 'Automix 03/05/2021'
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'AUTOMIX_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="No favorites found for users">

```js[Response]
{
    code: 404,
    message: {
        code: 'AUTOMIX_ERROR_NOT_FOUND_FAV_FOR_USERS',
    }
}
```

</base-code-block>
</template>
</endpoint>

## Export playlist medias

<endpoint command="exportPlaylistMedia" front="closed" auth user="admin">
<template #description>

Export playlist to a folder, creating a set of medias and .ass files so you can read your playlist with a regular media pleyer. 

A .m3u file is also created.

</template>
<template #request>

```js[Request]
{
    plaid: "451569d6-cc1c-4c33-a40c-7aa8a69f7a29",
    exportDir: "/home/aterizak/myPlaylist"
}
```

</template>
<template #success-response>

See getPlaylsitContents command, with an extra property called `exportedSuccessfully` to know if any media failed its export or not.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PL_EXPORT_MEDIA_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>
