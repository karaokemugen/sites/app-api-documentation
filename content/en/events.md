---
title: Websocket Events
description: Websocket Events you should listen to!
position: 99
category: Other
---

You can send information and commands to Karaoke Mugen through its endpoints, but sometimes you'll need to listen to Websocket events Karaoke Mugen sends to any user interface to warn of new information.

These are detailed here.

## adminMessage

### Data

```js[Response]
{
	destination: 'all' // 'all' | 'screen' | 'user'
	message: 'Karaoke is closing soon!'
	duration: 10000 //miliseconds
}
```

### Actions that may trigger this

- Operator sends a message either to the `all` or `user` destinations

### What you should do

- Display the message somewhere

## blacklistUpdated

### Actions that can trigger this

- Whitelist has been updated
- Blacklist criterias have been updated
- Current blacklist set has been changed
- Database generation has been triggered
- A repository has been updated

### What you should do

- Update the blacklist view if the user is seeing it
- Update the song list view since the blacklist update may hide/show new songs

## BLCSetInfoUpdated

A BLC Set information (name, mostly) has been modified.

### Data

You'll receive the BLC Set ID that has been modified.

```
2
```

### What you should do

- Update the BLC Set view if it's currently being viewed

Note : changing the current BLC Set will also trigger this, along with [`blacklistUpdated`](#blacklistUpdated)

## BLCSetsUpdated

the list of BLC Sets has been updated

### Actions that may trigger this

- A BLC Set has been added, removed, or edited

### What you should do

- Update the BLC Set list view

## currentPlaylistUpdated

### Data

You'll receive the playlist ID that has been modified.

```
657f71b0-6cd6-4763-8bfb-65620b9ed5ed
```

### Actions that may trigger this

- The current playlist has changed

### What you should do

- Switch the current playlist view to another playlist if a user is viewing it

## favoritesUpdated

### Data

You'll get the username of the user which favorites have been updated

```
axel@mykmrepo.net
```

### Actions that may trigger this

- A user has added or removed songs to its favorites

### What you should do

- Refresh the favorites view for this user

## downloadQueueStatus

### Data

You'll receive an array of `DBDownload` objects :

```js[Response]
[
	{
		name: string, // the song title usually goes there
		mediafile: string
		size: number, // in bytes
		uuid: string, // download UUID
		status?: 'DL_RUNNING' | 'DL_PLANNED' | 'DL_DONE' | 'DL_FAILED',
		repository: string,
		kid: string,
		started_at: Date
	}
]
```

### Actions that may trigger this

- The download queue has been updated (downloaded added, finished, failed, etc.)

Note : this doesn't trigger for download progress. See [`tasksUpdated`](#tasksUpdated) for that.

### What you should do

- Refresh the download queue view

## KIDUpdated

This event is used to send small updates so you can update the song list accordingly without having to re-query the whole thing.

### Data

You'll get an array of karaoke IDs with their updated data

```
[
	{
		kid: '657f71b0-6cd6-4763-8bfb-65620b9ed5ed',
		downloaded_status: 'DOWNLOADED'
	},
	{
		kid: 'fcbcc1e5-7bb6-45cf-b455-63f4e01061f1',
		flag_upvoted: true,
		username: 'axel@mykmrepo.net'
	}
]
```

### Actions that may trigger this

- A song's media has just been downlaoded
- Someone upvoted/downvoted a song
- A song has been added to the public playlist and shouldn't appear as 'addable' anymore
- A sonc has been removed from the public playlist and shouldn't appear as 'added' anymore

### What you should do

- Update only the songs mentionned if they are into view

## nextSong

### Data

```
{
	<Kara object>
}
```

### Actions that may trigger this

- A few seconds before the end of a song, you'll get infos about the next one.

Note : the operator might decide to change the next song at the last second

### What you should do

- Display a notification to non-operators only.

## noFreeSpace

### Actions that may trigger this

- Karaoke Mugen is trying to download a song but the disk space is not enough to allow that.

### What you should do

- Display a notification to operators only.

## operatorNotificationError

### Data

```
{
	code: 'NOTIFICATION.OPERATOR.ERROR.DOWNLOAD',
	data: <any>
}
```

### Actions that may trigger this

- An error has occured that only an operator should see. (Download failed, player is unable to read the next song, etc.)

### What you should do

- Display a notification to operators only.

## playerStatus

### Data

You'll receive a partial `PlayerStatus` object, which contains information that has changed since the last status update.

```
{
	position: 39
}
```

### Actions that may trigger this

- The player's status has been modified (position in file, file played, engine status, etc.)

### What you should do

- Update the player progress bar and controls

## playingUpdated

### Data

```
{
	plaid: '87088921-9f20-4f32-a0ac-9f2951b791a4',
    plc_id: 55
}
```

### Actions that may trigger this

- An operator or the engine switches the song currently playing on a playlist.

### What you should do

- Update the playlist view to highlight the song currently playing.

## playingContentsUpdated

### Data

```
87088921-9f20-4f32-a0ac-9f2951b791a4
```

### Actions that may trigger this

- A song has been added/removed/modified from a playlist

### What you should do

- Update the playlist view

## playlistImported

### Data

```
{
    plaid: '87088921-9f20-4f32-a0ac-9f2951b791a4'
    reposUnknown: ['world.karaokes.moe']
}
```

### Actions that may trigger this

- An operator drops a Playlist file into the Electron window, causing an `importPlaylist()` function to add a new playlist

### What you should do

- Update the playlists view
- If `reposUnknown` contains anything, offer the user to add it to its KM configuration.

## playlistInfoUpdated

### Data

```
87088921-9f20-4f32-a0ac-9f2951b791a4
```

### Actions that may trigger this

- A playlist information (duration, name, current/public/visible status, number of songs left to play) has been updated

### What you should do

- Update the playlist information on screen

## playlistsUpdated

### Actions that may trigger this

- A playlist has been added/removed/edited/imported

### What you should do

- Update the list of playlists

## publicPlaylistEmptied

This is a convenient event to let the user interface know all songs have been removed from the public playlist, and has a result, that all songs in the song library should have a addable button again without having to individually update each song and fire off many [`KIDUpdated`](#kidupdated) events.

### Actions that may trigger this

- The public playlist has been emptied by an operator

### What you should do

- Update all buttons on all songs in the song list to show a 'addable' sign again.

## publicPlaylistUpdated

### Actions that may trigger this

- The operator edited a playlist and set it as public

### What you should do

- Update the public playlist view

## quotaAvailableUpdated

### Data

```js[Response]
{
    username: 'axel@mykmrepo.net'
    quotaLeft: 2
    quotaType: 1
}
```

### Actions that may trigger this

- An user added a song to the public playlist
- A user's song has just been accepted into the current playlist
- A user's song has just been played
- An operator updated the quota settings

### What you should do

- Updated the number of songs/time left depending on the `quotaType`

## songPollEnded

### Actions that may trigger this

- A poll has ended and users cannot vote for it anymore
- Polls have been disabled by an operator while one was running

### What you should do

- Disallow voting

## songPollResult

### Data

Winner song

```js[Response]
{
    votes: 12,
    kara: 'Linda Linda',
    index: 2
}
```

### Actions that may trigger this

- A poll has ended successfully after its allowed time

### What you should do

- Display poll results

## songPollStarted

### Data

You'll get an array of `DBPLC` objects with two extra properties

```
[
	{
		...DBPLC,
		votes: 0, // Number of votes it has
		index: 0  // Index in the list
	},
	...
]
```

### Actions that may trigger this

- A new poll has started

### What you should do

- Display a song poll window asking the user what it wants to vote for

## songPollUpdated

### Data

```js[Response]
[
    {
        ...DBPLC object
        votes: 12,
        index: 0
    },
    ...
]
```

### Actions that may trigger this

- Someone voted for a song and it updated its number of votes

### What you should do

- Update poll results

## statsRefresh

### Actions that may trigger this

- Database has been regenerated
- Songs or tags have been added/removed

### What you should do

- Update database statistics

## tasksUpdated

### Data

You'll get an array of [`Task`](index#tasks) objects. This is useful to keep the user updated of long-running tasks in the background.

These tasks are usually updated frequently as they have detailed progress indicators for some.

If a task disappears from the array over updates, it means it has finished.

### Actions that may trigger this

- Download starting/being updated
- Database generation in progress
- Git update in progress

### What you should do

- Update task list for operators

## userSongPlaysIn

### Data

You'll get a `DBPLC` object.

### Actions that may trigger this

- Operator copied a song from the public playlist to the current one, meaning it'll play at some point in time.

### What you should do

- Notify the involved user (via the `username` property in the PLC object) when his song will play (via the `time_before_play` property)

## userUpdated

### Data

```
axel@mykmrepo.net
```

### Actions that may trigger this

- User profile has been updated (this can also happen from another session or via Karaoke Mugen Server's profile updates)

### What you should do

- Modify the user's name, avatar, etc. in playlists, user lists, etc.

## usersUpdated

### Actions that may trigger this

- A user has been added/removed from database

### What you should do

- Modify the user list

## whitelistUpdated

### Actions that may trigger this

- A song has been added/removed to the whitelist

### What you should do

- Refresh the whitelist view

## settingsUpdated

### Data

You'll receive a partial `Config` object with the updated settings.

### Actions that may trigger this

- An operator modified a particular setting

### What you should do

- Depending on the setting, you might have a few things to watch for, like interface mode changes, etc.





