---
title: Misc
description: Misc commands
position: 7
category: Endpoints
---

## Get Frontend Migrations

<endpoint command="getMigrationsFrontend" front="opened" auth user="admin">
<template #description>

Retrieve list of migrations with a flag to tell if they have been done or not. Frontend migrations are just pages the frontend loads with information for the user on new features or improtant notices. With a new installation all migrations are marked as done already.

</template>
<template #success-response>

```js[Response]
[
    {
        name: 'KMOnline'
        flag_done: false
    }
]
```

If a migration is false, you should display it to the user.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</base-code-block>
</template>

</endpoint>

## Set frontend migration

<endpoint command="setFrontendMigration" front="opened" auth user="admin">
<template #description>

Set a migration as done/undone.

</template>
<template #request>

```js[Request]
{
    mig: {
        name: 'KMOnline'
        flag_done: true
    }
}
```

</template>
<template #success-response>

Promise just ersolves

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</base-code-block>

</template>
</endpoint>

## Get remote status

<endpoint command="getRemoteData" front="open" auth user="admin">
<template #description>
Returns information about if the current app instance is available via remote access (through xxx.mykmrepo.net)
</template>
<template #success-response>

```js[Response]
{
	active: true,
    info: {
        host: 'mykmrepo.net',
        code: 'kkkk',
        token: 'blah',
    },
    token: 'remoteToken' // Token from the settings table
}
```

If `active` is `false`, no other information is returned.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</base-code-block>

</template>
</endpoint>

## Reset remote token

<endpoint command="resetRemoteToken" front="open" auth user="admin">
<template #description>
Resets the remote token used to enable remote access and communicate with a KM Server instance. Use this if the token has been compromised.

Actions taken by this command :

- destroy the remote access
- reinitialize token in settings table
- initialize new remote

</template>
<template #success-response>

Promise just resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
}
```

</base-code-block>

</template>
</endpoint>

## Shutdown application

<endpoint command="shutdown" front="open" auth user="admin">
<template #description>
Shutdowns the entire application.
</template>
</endpoint>

## Get Settings

<endpoint command="getSettings" front="closed">
<template #description>
Return settings, state and version.
</template>
<template #success-response>

```js[Response]
{
	version: {
        name: 'Axel Acrobate',
        number: '26.1.1',
        image: 'AxelAcrobate.jpg',
        sha: <git commit SHA>
    },
    config: { // Depending on user level, not all configuration is returned
        ...
    },
    state: { // Application state depending on user level
        currentPlaid: 1,
        publicPlaid: 1,
        appPath: '...' // only for admins, undefined for others
        dataPath: '...' // same
        os: 'win32' // same
        electron: // does the app run on electron or node ?
        defaultLocale: 'fr',
        supportedLyrics: [
            'ass',
            'srt',
            'kar',
            'txt',
            'kfn',
            'lrc'
        ],
        supportedMedias: [
            'avi',
            'mkv',
            'mp4',
            'webm',
            'mov',
            'wmv',
            'mpg',
            'm2ts',
            'rmvb',
            'ts',
            'm4v',
            'ogg',
            'm4a',
            'mp3',
            'wav',
            'flac',
            'mid'
        ],
        environment: 'production',
        sentrytest: false // Sentry is enabled if false.
        url: 'http://mykmrepo.net',
    }
}
```

For more information about configuration, see [sample file in the git repo](https://gitlab.com/karaokemugen/karaokemugen-app/-/blob/master/config.sample.yml)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
}
```

</base-code-block>

</template>
</endpoint>

## Update setting

<endpoint command="updateSettings" front="open" auth user="admin">
<template #description>
Updates one or more configuration options.
</template>
<template #request>

```js[Request]
{
    Frontend: {
        GeneratePreviews: false
    }
}
```

</template>
<template #success-response>

Configuration is returned as a result.

For more information about configuration, see [sample file in the git repo](https://gitlab.com/karaokemugen/karaokemugen-app/-/blob/master/config.sample.yml)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SETTINGS_UPDATE_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get displays

<endpoint command="getDisplays" front="closed" auth user="admin">
<template #description>
Return the list of displays the host computer has and their names. Only displays with positive resolutions are returned.
</template>
<template #success-response>

Returns an array of displays. See [systeminformation module's documentation](https://github.com/sebhildebrandt/systeminformation#6-graphics)

</template>
</endpoint>

## Get Stats

<endpoint command="getStats" front="closed" auth user="admin">
<template #description>
Get basic database statistics
</template>
<template #request>

```js[Request]
{
    tags: 15029,
    singers: 2039,
    songwriters: 8021,
    creators: 1122,
    authors: 250,
    karas: 11203,
    languages: 26,
    whitelist: 880, // number of songs whitelisted
    blacklist: 2, // number of songs blacklisted
    series: 5002,
    played: 12913 // number of songs played,
    playlists: 5,
    duration: 102391823908 // duration of all songs in database in seconds.
}
```

</template>
<template #success-response>

Request simply returns.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'STATS_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Refresh user quotas

<endpoint command="refreshUserQuotas" front="open" auth user="guest">
<template #description>
Forces a refresh of the current user's quota (uses the authorization token)
</template>
<template #success-response>

Promise resolves immediately, but you'll receive a [websocket event](events#quotaAvailableUpdated) once calculations are complete

</template>
</endpoint>

## Get player state

<endpoint command="getPlayerStatus" front="restricted" auth user="guest">
<template #description>
Returns the media player's state
</template>
<template #success-response>

```js[Response]
{
    volume: 98,
    playing: true,
    playerStatus: 'play', // 'stop' or 'pause' too,
    _playing: true, // internal delay flag,
    timeposition: 57.2, // in seconds
    mute: false,
    currentSong: {
        ...PlaylistContent,
        avatar: '...' // Path to avatar file ,
        infos: '...' // ASS script to display song information in the player
    },
    currentMedia: { // If a playlist media is playing instead of a song
        series: 'One Piece',
        filename: 'One Piece - EC1.mp4',
        type: 'Jingles'
    },
    mediaType: 'song',
    showSubs: true,
    onTop: true,
    fullscreen: true,
    border: true,
    url: 'http://abcd.mykmrepo.net',
    monitorEnabled: false // Second player monitor enabled?
    songNearEnd: false // Is the song about to end (8 seconds left or less),
    nextSongNotifSent: false,
    isOperating: false // Player lock status to avoid sending multiple commands
    currentSessionID: <uuid>, // Current karaoke session,
    currentRequester: 'Axel', // Who asked the current song,
    stopping: false // true if player is going to stop after current song
    defaultLocale: 'fr',
    songsBeforeJingle: 22,
    songsbeforeSponsors: 42
}
```

`currentMedia.type` can be :

- `Jingles`
- `Sponsors`,
- `Outros`,
- `Intros`,
- `Encores`

`mediaType` can be :

- one of `currentMedia.type`
- `song`
- `background` : when the player is stopped
- `pauseScreen` : when the player is inbetween songs in classic or streamer mode

</template>
</endpoint>

## Get news feed

<endpoint command="getNewsFeed">
<template #description>
Returns a news feed from the lab and KM's mastodon account.

</template>
<template #success-response>

```js[Response]
[
    {
        name: 'git_base',
        body: <stringified JSON>
    },
    {
        name: 'git_app',
        body: <stringified JSON>
    },
    {
        name: 'mastodon',
        body: <stringified JSON>
    }
]
```

For info on feeds, [check the feed tool code](https://gitlab.com/karaokemugen/karaokemugen-app/-/blob/master/src/services/proxyFeeds.ts#L11)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
}
```

</base-code-block>

</template>
</endpoint>

## Get catchphrase

<endpoint command="getCatchphrase" front="open" auth user="guest">
<template #description>
Returns a random catchphrase string
</template>
<template #success-response>

Response is in plain text.

```js[Response]
Blablabla
```

</template>
</endpoint>

## Get logs

<endpoint command="getLogs" front="closed" auth user="admin">
<template #description>
Get Karaoke Mugen system logs.

This aligns logs from websocket to the ones you'll have received through this route.

</template>
<template #request>

```js[Request]
{
    level: 'info'
}
```

</template>
<template #success-response>

```js[Response]
[
    {
        level: 'info',
        service: 'Player',
        message: 'Hey! Listen!',
        timestamp: '2021-03-03T15:43:52.755Z',
        obj: {} // optional detail object, usually for errors
    }
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'ERROR_READING_LOGS'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Backup settings

<endpoint command="backupSettings" front="closed" auth user="admin">
<template #description>
Saves configuration as another file `config.backup.yml`

</template>
<template #success-response>

```js[Response]
[
    code: 200,
    message: {
        code: 'CONFIG_BACKUPED'
    }
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'CONFIG_BACKUPED_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Generate database

<endpoint command="generateDatabase" front="opened" auth user="admin">
<template #description>
Triggers a new database generation from the current data files.

</template>
<template #success-response>

A new [Task](index#Tasks) will start with progress immediately after the call is made.

Once completed you'll get :

```js[Response]
{
    code: 200,
    message: {
        code: 'DATABASE_GENERATED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'DATABASE_GENERATED_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Validate files

<endpoint command="validateFiles" front="opened" auth user="admin">
<template #description>
Triggers a data files validation process. Files are read and verified. Any inconsistency is fixed (wrong subchecksum, wrong media metadata, etc.) Useful when files have been modified from outside Karaoke Mugen.

</template>
<template #success-response>

A new [Task](index#Tasks) will start with progress immediately after the call is made.

Once completed you'll get :

```js[Response]
{
    code: 200,
    message: {
        code: 'FILES_VALIDATED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'FILES_VALIDATED_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Dump database

<endpoint command="dumpDatabase" front="opened" auth user="admin">
<template #description>
Dumps database to a SQL file. This works only on installation relying on the bundled PostgreSQL server.

The file is saved as `karaokemugen.sql` in `dataPath`.

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'DATABASE_DUMPED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'DATABASE_DUMPED_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Restore database

<endpoint command="restoreDatabase" front="opened" auth user="admin">
<template #description>
Restores database from the `karaokemugen.sql` file in `dataPath`.

WARNING: This obviously destroys any data not dumped previously.

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'DATABASE_RESTORED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'DATABASE_RESTORED_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get directory from filesystem

<endpoint command="getFS" front="opened" auth user="admin">
<template #description>
Returns a list of files and folders in the choosen `path`. Useful to provide browsing capabilities to your app without Electron's native open file dialogs.
</template>
<template #success-response>

```js[Response]
{
    contents: [
        {
            name: 'KaraokeMugen',
            isDirectory: true
        },
        ...
    ]
    drives: [
        // Windows only.
    ],
    fullPath: 'C:\Users\something\'
}
```

For `drives`, please refer to [systeminformation's documentation](https://github.com/sebhildebrandt/systeminformation#9-file-system) : `blockDevices`


</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'FS_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get Electron Versions

<endpoint command="getElectronVersions" front="opened" auth user="guest">
<template #description>
Returns `process.versions` from Electron.
</template>
<template #success-response>

See Electron and Node's API for `process.versions`.

</template>
</endpoint>

## Get audio devices

<endpoint command="getAudioDevices" front="closed" auth user="admin">
<template #description>

Returns a list of audio devices MPV supports

</template>
<template #success-response>

[
  ['auto', 'Autoselect device'],
  ['wasapi/{d5df6e9d-1ff9-4318-bc82-a79bed31abb0}', 'Haut-parleurs (Realtek(R) Audio)'],
  ['openal', 'Default (openal)'],
  ['sdl' 'Default (sdl)']
]

</template>
</endpoint>

## Open current log file

<endpoint command="openLogFile" front="closed" auth user="admin">
<template #description>

Stats a file explorer on the host computer with the current log file selected.

On Linux, it'll try `xdg-open` then `nautilus` and then `dolphin` before giving up.

</template>
<template #success-response>

Simply resolves.

</template>
</endpoint>
