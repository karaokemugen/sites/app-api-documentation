---
title: Introduction
description: About KMApp Protocol (socket.io based)
position: 1
category: Intro
---

<alert type="info">

Please read this before exploring all endpoints.

</alert>

<alert type="info">
<p class="flex items-center">This documentation is available in dark mode! <app-color-switcher class="inline-flex ml-2"></app-color-switcher></p>
</alert>

## Protocol

Karaoke Mugen App uses a [**Socket.IO** *ver. 3*](https://socket.io/) connection to handle *all* API requests, it also carries events about currently playing song and playlist or users updates.

The same protocol is used by Karaoke Mugen Server to provide remote control and shortener.

Each request has its associated event and [is acked by the server](https://socket.io/docs/v3/emitting-events/#Acknowledgements) whenever the request is fulfilled.

`socket.emit('CommandName', {...}, res => {})`

### Anatomy of a request

Each request is populated with, if needed, credentials and a payload (`body`).

```js[RequestExample]
{
	// Request payload, can be of any type
	body: {}
	// JWT token (local authorization, obtained by using login endpoint)
	authorization: ''
	// JWT Token (remote authorization, KMServer)
	onlineAuthorization: ''
}
```

### Anatomy of a response

Each answer is formatted like this `{ err: boolean, data?: any }`. If `err` is `true`, *most of the time*, the error will be formatted with a numeric code (similar to what HTTP err codes can mean) and a message.

<base-code-group>
<base-code-block label="Success" active>

```js
{
	err: false,
	data: {
		// ...
	}
}
```

</base-code-block>
<base-code-block label="Error">

```js
{
	err: true,
	data: {
		code: 404,
		message: {
			code: 'UNKNOWN_COMMAND'
		}
	}
}
```

</base-code-block>
<base-code-block label="Frontend is closed/restricted">

This error happens when frontend is closed or restricted and you're using an unavailable API.

```js
{
	err: true,
	data: {
		code: 503,
		message: {
			code: 'WEBAPPMODE_CLOSED_API_MESSAGE'
		}
	}
}
```

</base-code-block>
<base-code-block label="Not enough privileges">

This error happens if you're requesting a resource that isn't available for your user type.

If requesting an admin resource and you're a user :

```js
{
	err: true,
	data: {
		code: 403,
		message: {
			code: 'ADMIN_PLEASE'
		}
	}
}
```

If requesting any resource a guest isn't supposed to access and you're a guest :

```js
{
	err: true,
	data: {
		code: 403,
		message: {
			code: 'NO_GUEST'
		}
	}
}
```

</base-code-block>
</base-code-group>

### Requests and response format

Requests of all endpoints will be described as contents of the `body` object.

Responses from all endpoints will be in the `data` object for the response.

### Example

```js[login.js]
import { io } from 'socket.io-client';

const socket = io();

socket.emit('login', {
	body: { username: 'hello', password: 'W0rld!!!' }
}, res => {
	console.log(res);
	/*
		{
			err: false,
			data: {
				token: '',
				// ...
			}
		}
	*/
});
```

## REST API access

This allows you to send an API command through a REST endpoint, if you don't want to use websockets.

<endpoint command="/api/command" front="closed">
<template #description>
Send a Socket API command and get its results.
</template>
<template #request>

```js[Request]
{
    cmd: 'getKaras',
	body: {
		body: {
			filter: 'Dragon Ball Z'
		},
	},
	authorization: 'abcdefghijkl...'
}
```

</template>
<template #success-response>

```js[Response - 200 OK]
See getKara response in API documentation.
```

</template>
<template #errors>
<base-code-block label="Generic error">

```js[Response - 500 Internal Server Error]
{
    ...
}
```

</base-code-block>
</template>
</endpoint>

## Uploading files

Uploading files is not done through the Websocket API as it tends to overload the protocool with big files, so we use instead a REST endpoint for that.

<endpoint command="/api/importfile" front="closed">
<template #description>
Upload a file
</template>
<template #request>

Send the file as a `file` form item.

```js[Request]
{
    file: <data>
}
```

</template>
<template #success-response>

```js[Response - 200 OK]
{
	originalname: 'avatar.png' // Name on the user's computer,
	filename: 'abcdefghijklmn' // Name on the server
}
```

</template>

## Tasks

Tasks are special websocket events to listen to. They're used by the backend to keep the frontend updated with long-running tasks in the background like downloads, database generation or other things.

For more information, check out the [events API](events)