---
title: Authentication
description: Authentication endpoint description
position: 3
category: Endpoints
---

## Login

<endpoint command="login" front="closed">
<template #description>

Login using either a local or an online account. Supply optional security code to promote user.

</template>
<template #request>

```js[Request]
{
    username: 'hello',
    password: 'superpassword',
    securityCode: 134455
}
```

</template>
<template #success-response>

```js[Response]
{
    token: '', // To be used in requests when needed
    username: 'hello',
    role: 'admin', // guest, user or admin (local role)
    onlineToken: '' // Only present if the user is online
}
```

</template>
<template #errors>
<base-code-block label="Invalid credentials">

```js[Response]
{
    code: 401,
    message: {
        code: 'LOG_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Login as Guest

<endpoint command="loginGuest" frontend="closed">
<template #description>Login as a random guest account. If `Frontend.AllowCustomTemporaryGuests` is enabled, you can send a `name` property to name your own guest.</template>
<template #request>

```js[Request]
{
    name: 'Alex Teriyaki The Second'
}
```

</template>
<template #success-response>

```js[Response]
{
    token: '', // To be used in requests when needed
    username: 'hello',
    role: 'guest', // guest, user or admin (local role)    
}
```

</template>
<template #errors>
<base-code-block label="No more guests">

```js[Response]
{
    code: 500,
    message: {
        code: 'NO_MORE_GUESTS_AVAILABLE'
    }
}
```

</base-code-block>
<base-code-block label="Guests not allowed">

```js[Response]
{
    code: 403,
    message: {
        code: 'GUESTS_NOT_ALLOWED'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Check Auth Token

<endpoint command="checkAuth" front="closed">
<template #description>Verify if your token is still valid.</template>
<template #request>

```js[Request]
{
    // Empty payload, just pass your tokens
}
```

</template>
<template #success-response>

```js[Response]
{
    token: '', // To be used in requests when needed
    username: 'hello',
    role: 'guest', // guest, user or admin (local role)    
}
```

</template>
<template #errors>
<base-code-block label="No more guests">

```js[Response]
{
    code: 500,
    message: {
        code: 'NO_MORE_GUESTS_AVAILABLE'
    }
}
```

</base-code-block>
</template>
</endpoint>
