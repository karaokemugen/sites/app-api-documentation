---
title: Player
description: Player commands
position: 8
category: Endpoints
---

## Start playing song (classic mode)

<endpoint command="play" front="restricted" auth user="guest">
<template #description>

Start the player. This is only allowed for guests and users if it's their turn to play (the song's requester equals the user accessing this command)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 403,
    message: {
        code: "USER_NOT_ALLOWED_TO_SING
    }
}
```

</base-code-block>
</template>
</endpoint>

## Display message on the screen

<endpoint command="displayPlayerMessage" front="opened" auth user="admin">
<template #description>

Display a message on screen or user devices

</template>
<template #request>

```js[Request]
{
    message: 'Free drinks for one hour!',
    duration: 10000, //in miliseconds
    destination: 'screen' 
}
```

`destination` options : 

- `screen` : Display message on the player's video screen
- `users` : Display as a toast notification on user devices
- `all` : Display on both screen and user devices

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'MESSAGE_SENT'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'MESSAGE_SEND_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,
    message: {
        // validation errors here
    }
}
```

</base-code-block>
</template>
</endpoint>

## Send player command

<endpoint command="sendPlayerCommand" front="closed" auth user="guest">
<template #description>
Send a command to the player. See request for details.

Commands are available to guests only if `Frontend.PublicPlayerControls` is enabled in config.

</template>
<template #request>

```js[Request]
{
    command: 'play',
    options: null    
}
```

`command` options : 

- `play` : Starts or resume playback
- `stopNow` : Stop playback NOW
- `pause` : Pauses playback
- `stopAfter` : Stop playback after current song ends
- `skip` : Skip to next song
- `prev` : Go to previous song
- `toggleFullscreen` : Toggle player's full screen mode
- `toggleAlwaysOnTop` : Toggle player's always on top mode
- `toggleBorders` : Toggle player's window borders
- `setHwDec` : Set hardware decoding option : `no`, `yes` or `auto-safe` (default),
- `mute` : Mute all sounds
- `unmute` : Unmute player
- `showSubs` : Show lyrics on screen
- `hideSubs` : Hide lyrics on screen
- `seek` : Go forwards or backwards in the song. `options` is a delta in seconds (like `-2` or `10` to go 10 seconds forwards)
- `goTo` : Go to exact position in song. `options` is the position in seconds.
- `setVolume` : Set current volume

</template>

<template #success-response>

```js[Response]
{
	code: 200,
    message: {
        code: 'STOP_AFTER'
    }    
}
```

Only `stopAfter` adds a `message` object in the response.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'COMMAND_SEND_ERROR',
        data: 'Command seek must have a numeric option value'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Start up video player

<endpoint command="startPlayer" front="open" auth user="admin">
<template #description>
Start video player. This command is used only after setup is completed so not to start the video player until the user has completed the app's onboarding process.

</template>
<template #success-response>

Promise just resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
}
```

</base-code-block>
<base-code-block label="Player is already running">

```js[Response]
{
    code: 409,    
}
```

</base-code-block>
</template>
</endpoint>