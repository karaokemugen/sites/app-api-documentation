---
title: Backgrounds
description: Manage background and music during pauses
position: 15
category: Endpoints
---

## Get background files

<endpoint command="getBackgroundFiles" front="closed" auth user="admin">
<template #description>

Get a list of background files on the computer hosting KM.

</template>
<template #request>

```js[Request]
{
    type: 'pause' | 'stop' | 'poll' | 'bundled'
}
```

* `pause` : Pause between songs
* `stop` : When the stop button is used or the player is initialized
* `poll` : During song polls
* `bundled` : Lists bundled files with this specific version of Karaoke Mugen

</template>
<template #success-response>

```js[Response]
{
    pictures: [
		'a.jpg',
		'b.jpg'
	],
	music: [
		'a.mp3'
	]
}
```


</template>
</endpoint>

## Add background file

<endpoint command="addBackground" front="closed" auth user="admin">
<template #description>
Add a background file to a specific type of background. Background files can be music or pictures. 
</template>
<template #request>

```js[Request]
{
    type: 'poll'
	file: 'abcdefghoklj'
}
```

See [Get Background Files](#get-background-files) for more info on which types can be used.

`file` is the object you get after uploading a file via the upload route. It should contain `filename` and `originalname` properties like when you upload songs.

* `filename` : is the filename in the temp directory after upload
* `originalname` : is the name of the file on the user's computer

</template>
<template #success-response>

No specific response is sent other than success.

</template>
</endpoint>

## Remove background

<endpoint command="removeBackground" front="closed" auth user="admin">
<template #description>
Remove a background file
</template>
<template #request>

```js[Request]
{
    type: 'stop'
	file: 'abcdefghijk.jpg'
}
```

</template>
<template #success-response>

No specific response is sent other than success.

</template>
</endpoint>
