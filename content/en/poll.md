---
title: Polls
description: Polls management
position: 10
category: Endpoints
---

## Get current poll

<endpoint command="getPoll" front="restricted" auth user="guest">
<template #description>

Get the current poll contents if one is running

</template>
<template #success-response>

```js[Response]
{
    infos: {
        count: 5
        from: 0
        to: 5
    },
    poll: [
        {
            <See PLC item>
            votes: 3 //number of votes
            index: 0 //vote option index
        },
        ...
    ],
    timeleft: 51 // seconds left before poll ends
    flag_uservoted: false // has user voted already?
}
```

</template>
<template #errors>
<base-code-block label="Poll inactive">

```js[Response]
{
    code: 425,
    message: {
        code: 'POLL_NOT_ACTIVE'
    }
}
```

</template>
</endpoint>

## Vote in a poll

<endpoint command="votePoll" front="restricted" auth user="guest">
<template #description>
Vote in a poll.
</template>
<template #request>

```js[Request]
{
    index: 0 // vote option index, see getPoll
}
```

</template>
<template #success-response>

```js[Response]
{
	code: 200,
    message: {
        code: 'POLL_VOTED',
        data: <See poll object in getPoll>
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'POLL_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Poll not active">

```js[Response]
{
    code: 425,
    message: {
        code: 'POLL_NOT_ACTIVE'
    }
}
```

</base-code-block>
<base-code-block label="User has already voted">

```js[Response]
{
    code: 429,
    message: {
        code: 'POLL_USER_ALREADY_VOTED'
    }
}
```

</base-code-block>
<base-code-block label="Poll item unknown">

```js[Response]
{
    code: 404,
    message: {
        code: 'POLL_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad Request">

```js[Response]
{
    code: 400,
    message: {
        // validation errors
    }    
}
```

</base-code-block>
</template>
</endpoint>