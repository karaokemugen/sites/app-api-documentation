---
title: Quiz Games
description: Quiz Games management
position: 16
category: Endpoints
---

## Start a game

<endpoint command="startGame" front="closed" auth user="admin">
<template #description>

Starts a new game

<template #request>

If `loadSettingsFromDB` is set to `true`, settings for that game will be loaded into config from the DB. If `false`, 

```js[Request]
{
    gamename: 'Karaoke Quiz Time!'
	loadSettingsFromDB: false
}
```

</template>
<template #success-response>

Simply resolves

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
}
```

</template>
</endpoint>

## Stop current game

<endpoint command="stopGame" front="closed" auth user="admin">
<template #description>
Stops the current game, returning the app to normal karaoke operating mode.
</template>
<template #success-response>

Simply resolves

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</base-code-block>

</template>
</endpoint>

## Delete a game

<endpoint command="deleteGame" front="closed" auth user="admin">
<template #description>

Removes a game from database

<template #request>

```js[Request]
{
    gamename: 'Karaoke Quiz Time!'
}
```

</template>
<template #success-response>

Simply resolves

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
}
```

</template>
</endpoint>

## Reset game scores

<endpoint command="resetGameScores" front="closed" auth user="admin">
<template #description>

Resets all scores from a game to start anew.

<template #request>

```js[Request]
{
    gamename: 'Karaoke Quiz Time!'
}
```

</template>
<template #success-response>

Simply resolves

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
}
```

</template>
</endpoint>

## Toggle game continue

<endpoint command="continueGameSong" front="closed" auth user="admin">
<template #description>

???

<template #success-response>

Simply resolves

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
}
```

</template>
</endpoint>

## Get list of games

<endpoint command="getGames" front="closed" auth user="admin">
<template #description>

List all games in database to choose oen from.

<template #success-response>

```js[Response]
[
	{
		gamename: 'Karaoke Quiz Time!',
		settings: {
			Playlist: <uuid>,
			EndGame: {
				MaxScore: {
					Enabled: true, // Game ends when a player reaches score X
					Score: 10
				},
				MaxSongs: {
					Enabled: false,
					Songs: 0
				},
				Duration: {
					Enabled: true, // Game can also end if duration exceeds 60 minutes
					Minutes: 60
				}
			},
			Players: {
				Twitch: true,              // Twitch chat is allowed to participate
				TwitchPlayerName: 'Twitch' // Name of the twitch channel's player on the leaderboard
				Guests: false              // Guests not allowed
			},
			TimeSettings: {
				WhenToStartSong: 15,       // Percentage of when to start the song
				GuessingTime: 25,          // Seconds to guess 
				QuickGuessingTime: 10,     // Give extra points if answer is given before that time in seconds
				AnswerTime: 30             // Show answer for 30 seconds before going to the next song
			},
			Answers: {
				Accepted: {
					series: {
						Enabled: true,
						Points: 1
					},
					songwriters: {
						Enabled: true,
						Points: 5
					}
				}, // All tag types + title + year are accepted
				QuickAnswerAwardedPoints: 5,           // Number of points for a quick good answer
				SimilarityPercentageNeeded: 80         // Percentage of how much similar the answer given needs to be to the real answer to award points
			},
			Modifiers: { // What to hide 
				Mute: false,
				Blind: 'black', // Also 'blur' or 'none'
				NoLyrics: true,
				Pitch: 0
				Speed: 0
			},
			RandomHide: 0     // If >0 uses this number of modifiers randomly for each song
		},
		state: {
			running: true,
			currentSongNumber: 5,        // song currently on
			currentTotalDuration: 1200,  // seconds
			playlist: <uuid>,
			currentSong: <kara object>
		},
		date: <Date>,
		flag_active: true // is it the currently active game?
	}, ...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,    
}
```

</template>
</endpoint>

## Get user's game score

<endpoint command="getGameScore" front="closed" auth user="guest">
<template #description>

Returns a specific user's game score list. If the game had 10 songs, you'll get 10 entries.

<template #request>

```js[Request]
{
    gamename: 'Karaoke Quiz Time!',
	username: 'axel@mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Response]
[
	{
		nickname: 'Axel Terizaki',
		kid: '55688483-19b8-4268-9691-9052ac88ac1a', // Full answer
		answer: 'Kaerimichi',                       // User entered this
		points: 3,                                  // points awarded,
		gamename: 'Karaoke Quiz Time!'
	}, ...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</template>
</endpoint>

## Get game's total scores

<endpoint command="getTotalGameScore" front="closed" auth user="guest">
<template #description>

Returns a leaderbooard with a game's scores for each player involved.

Entries are sorted by best score first.

<template #request>

```js[Request]
{
    gamename: 'Karaoke Quiz Time!'	
}
```

</template>
<template #success-response>

```js[Response]
[
	{
		nickname: 'Axel Terizaki',
		total: 50
	},
	{
		nickname: 'QCTX',
		total: 47
	}
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</template>
</endpoint>

## Get possible answers

<endpoint command="getPossibleAnswers" front="closed" auth user="guest">
<template #description>

Returns an array of possible answers to choose from depending on what the user typed. Like a search engine allowing players to select the answer they want from what they typed.

<template #request>

```js[Request]
{
    answer: 'Mahoro'
}
```

</template>
<template #success-response>

```js[Response]
[
	{
		ktid: '<uuid>',              // Can be either a Kara ID or Tag ID
		type: null                   // Tag type if applicable,
		default_name: 'Mahoromatic', // Tag name if applicable
		default_language: 'eng',     // Default language to display the name to the user
		i18n: {...}                  // i18n object with language = name
	}, ...	
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</template>
</endpoint>

## Give an answer

<endpoint command="setAnswer" front="opened" auth user="guest">
<template #description>

Give answer for a specific player. The player identity is defined by its auth token. Answers can be either specific TIDs or KIDs, but also simple strings, depending on what kind of answers are accepted.

<template #request>

```js[Request]
{
    anser: <uuid>
}
```

</template>
<template #success-response>

Simply resolves

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</template>
</endpoint>

## Get quiz timers

<endpoint command="getQuizTimers" front="opened" auth user="guest">
<template #description>

Get remaining time for all different timers.

<template #success-response>

- `guessTime`: Time left for the players to guess the song playing. 0 if the time is over already
- `revealTime`: Time left for the reveal of the song players had to guess. 
- `quickGuess`: Time left before answers can be considered a "quick guess". A period of time that allows players to earn bonus points.

```js[Response]
{
	guessTime: 0,
	revealTime: 12,
	quickGuess: 0
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</template>
</endpoint>