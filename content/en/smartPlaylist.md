---
title: Smart Playlists
description: Smart Playlist management
position: 10
category: Endpoints
---

**Note :** any actions that modify criterias set in any way (add/edit/remove criterias, change active set, etc.) will trigger a smart playlist regeneration

## Types of criterias

### Expected values

* Types `1` to `999` take tag ID numbers
* Types `0`, `1000`, and `1004` take strings
* Type `1006` takes either `MISSING`, `DOWNLOADED` or `DOWNLOADING` (but why would you want to blacklist downloaded or downloading songs?)
* Type `1001` takes karaoke ID numbers
* Types `1002`, `1003` take numeric values.

### Types description

* `0` : Ban on year.
* `1` : Ban on series.
* `2` : Ban on singer.
* `3` : Ban on songtype.
* `4` : Ban on creator.
* `5` : Ban on language.
* `6` : Ban on author
* `7` : Ban on misc tags
* `8` : Ban on songwriter
* `9` : Ban on downloadable groups
* `10` : Ban on family
* `11` : Ban on origin
* `12` : Ban on genre
* `13` : Ban on platform
* `14` : Ban on version
* `15` : Ban on warning
* `16` : Ban on collection
* `1000` : Ban on series' name. Example : `Gundam` will ban `Mobile Suit G Gundam` and  `Gundam SEED`
* `1001` : Ban on exact karaoke ID. Used to ban specific karaoke songs. Value should contain a single karaoke ID.
* `1002` : Ban song if longer than xx seconds.
* `1003` : Ban song if shorter than xx seconds.
* `1004` : Ban on song title's name. Example : `Mambo` will ban `Mahoro de Mambo` and any other song containing `Mambo`
* `1005` : Ban on the tag's name. Example : `Nana` will ban all songs with a tag containing `Nana`
* `1006` : Ban on download status

## Empty criterias

<endpoint command="emptyCriterias" front="closed" auth user="admin">
<template #description>

Empty criterias for a playlist

</template>
<template #request>

```js[Request]
{
    plaid: 'abcdefgh'
}
```

</template>
<template #success-response>

No message is sent, promise is simply resolved.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'CRITERIAS_EMPTY_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Playlist unknown">

```js[Response]
{
    code: 404,
    message: {
        code: 'CRITERIAS_EMPTY_ERROR',
        data: 'Playlist unknown'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Get list of criterias

<endpoint command="getCriterias" front="open" auth user="admin">
<template #description>
List all criterias from a specific smart playlist
</template>
<template #request>

```js[Request]
{
	plaid: 'abcdefghj'
}
```

</template>
<template #success-response>

```js[Response]
[
	{
		"type": 6,
		"value": "500030d3-8600-4728-b367-79ff029ea7c9",
		"value_i18n": "Jean-Jacques Debout",
		"plaid": "abcdefghj"
	}
]

```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'CRITERIAS_VIEW_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Add criterias

<endpoint command="addCriterias" front="open" auth user="admin">
<template #description>
Add new criterias to a specific smart playlist
</template>
<template #request>

```js[Request]
[
	{
		"type": 6,
		"value": "500030d3-8600-4728-b367-79ff029ea7c9",
		"value_i18n": "Jean-Jacques Debout",
		"plaid": "abcdefghj"
	}
]
```

</template>
<template #success-response>

Simply returns

</template>
<template #errors>
<base-code-block label="Bad Request">

```js[Response]
{
    code: 400,
    message: {
        code: 'CRITERIA_ADD_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Playlist not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'CRITERIA_ADD_ERROR'
    }
}
```

</base-code-block>

<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'CRITERIA_ADD_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Delete criteria

<endpoint command="removeCriterias" front="open" auth user="admin">
<template #description>
Remove criterias
</template>
<template #request>

```js[Request]
[
	{
		"type": 6,
		"value": "500030d3-8600-4728-b367-79ff029ea7c9",
		"value_i18n": "Jean-Jacques Debout",
		"plaid": "abcdefghj"
	}
]
```

</template>
<template #success-response>

Request simply returns.

</template>
<template #errors>
<base-code-block label="Playlist not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'CRITERIA_DELETE_ERROR'
    }
}
```

</base-code-block>

<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'CRITERIA_DELETE_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Create problematic Smart Playlist

<endpoint command="createProblematicSmartPlaylist" front="closed" auth user="admin">
<template #description>
This creates a smart playlist with only problematic tags inside (like R18, Spoiler, etc.)

This route is only used on the app's first run after setup is complete and all songs and tags have been downloaded.
</template>
<template #success-response>

This simply returns.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'BLC_PROBLEMATIC_SET_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>
