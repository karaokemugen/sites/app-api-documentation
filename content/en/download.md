---
title: Downloads
description: Manage downloads and song updates
position: 10
category: Endpoints
---

**Note :** BLC means Blacklist Criteria

## Add download to queue

<endpoint command="addDownloads" front="closed" auth user="admin">
<template #description>

Add a series of download items to the download queue. Download queue starts as soon as items are in it.

</template>
<template #request>

```js[Request]
[
    {
        kid: '1b15d050-0394-4f55-ae07-ea2b014b90e8',
        // name is what's displayed in the queue
        name: 'ENG - Daft Punk - MV - Harder better faster stronger',
        size: 123901239,
        repository: 'mykmrepo.net'
    },
    ...
]
```

</template>
<template #success-response>

```js[Response]
{
    code: 'DOWNLOADS_QUEUED',
    data: 1 //number of songs queued
}
```

</template>
<template #errors>
<base-code-block label="Error">


</base-code-block>
<base-code-block label="Songs already in queue">

```js[Response]
{
    code: 409,
    message: {
        code: 'DOWNLOADS_QUEUED_ALREADY_ADDED_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Get download queue status

<endpoint command="getDownloadQueueStatus" front="closed" auth user="admin">
<template #description>
Receive a string giving you the status of the download queue.
</template>
<template #success-response>

Possible responses :

- `stopped`
- `started`
- `paused`
- `updated`

</template>
</endpoint>

## Get queued downloads

<endpoint command="getDownloads" front="restricted" auth user="guest">
<template #description>
Get the list of downloads currently in queue.
</template>
<template #success-response>

Possible statuses for downloads :

- `DL_RUNNING` : Currently being downloaded
- `DL_PLANNED` : In queue, planned for download
- `DL_DONE` : Download successful
- `DL_FAILED` : Download failed

```js[Response]
[
  {
    name: "ENG - Cowboy Bebop - OP - Tank",
    urls: {
        media: {
            remote: "http://xxx/downloads/ENG - Cowboy Bebop - OP - Tank.mp4",
            local: "ENG - Cowboy Bebop - OP - Tank.mp4"
 		}
 		...
 	},
 	size: 12931930,
 	uuid: "3e1efc1c-e289-4445-8637-b944a6b00c6f",
 	started_at: 2019-12-31T01:21:00
 	status: "DL_PLANNED"
  },
  ...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'DOWNLOADS_GET_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Wipe download queue

<endpoint command="deleteDownloads" front="open" auth user="admin">
<template #description>
Remove ALL downloads from queue
</template>
<template #success-response>

Just resolves in case of success.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'DOWNLOADS_WIPE_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Pause download queue

<endpoint command="pauseQueue" front="open" auth user="admin">
<template #description>
Pauses download queue. The currently downloading item will need to finish though as it cannot be aborted.
</template>
<template #success-response>

Just resolves in case of success.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'DOWNLOADS_PAUSE_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Start download queue

<endpoint command="startDownloadQueue" front="open" auth user="admin">
<template #description>

</template>
<template #success-response>

```js[Response]
{
    code: 'DOWNLOADS_STARTED'
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 'DOWNLOAD_START_ERROR'
}
```

</base-code-block>

</template>
</endpoint>

## Sync only medias from remote repositories

<endpoint command="updateAllMedias" front="open" auth user="guest">
<template #description>
This action will only download all medias from configured repositories. It will not download any karaoke metadata or lyrics.

This is useful if you are keeping your songs/lyrics synced via git or any other mean and only want to update medias.

This downloads new/updated medias, and removes old ones.

This is done asynchronously.

</template>
<template #success-response>

```js[Response]
{
    code: 'UPDATING_MEDIAS_IN_PROGRESS'
}
```

</template>
</endpoint>
