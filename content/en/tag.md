---
title: Tags
description: Get, add, edit tags
position: 13
category: Endpoints
---

## Tag types

See [the tagType object from the shared lib](https://gitlab.com/karaokemugen/lib/-/blob/master/utils/constants.ts#L59). Each type is self-explanatory and has a number ID.

## Get tags

<endpoint command="getTags" front="restricted" auth user="guest">
<template #description>

Query list of tags in the database

</template>
<template #request>

```js[Request]
{
    type: 5,         // filter tags by a certain type (language here)
    filter: 'fre',   // filter by a string
    from: 0,         // where to start, use for pagination
    size: 30,        // how many records, use for pagination,
    stripEmpty: true // Strip tags with 0 songs attached to them
}
```

</template>
<template #success-response>

This return an object with several important properties.

- `infos` contains information about the results. Number of total results, from and to which (for pagination)
- `content` contains a tag object. See example response for details
  
```js[Response]
{
    "content": [
      {
        tid: 'abcdef',
        types: [2, 5],               // Tag belongs to two types
        name: 'My tag name',
        short: 'TAG'                 // Short (3 or 4 characters-long) name
        i18n: {
            fre: 'lol',
            eng: 'lol'
        },
        aliases: [                   // Aliases are used for search only
            'MTN'
        ],
        karacount: [                 // Number of songs the tag is in
            { type: 2, count: 523 }, // This is by type the tag belongs to
            { type: 5, count: 10 }
        ],
        tagfile: 'My tag name.abcdef.tag.json',
        repository: 'mykmrepo.net',
        karafile_tag: null, // A small tag to add to kara file names if this tag is in it.
        noLiveDownload: false        // forbid any song with this tag to be viewable/downloadable on KM Explorer,
        priority: 5,                  // Low priority means the tag is displayed first if several tags of the same type are on the same song. -1 means it's hidden from public view,
        description: {
            eng: 'This is a tag description'
        }
      }
    ],
    infos: {
        count: 201,
        from: 0,
        to: 201
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'TAG_LIST_ERROR'
    }
}
```

</template>
</endpoint>

## Create tag

<endpoint command="addTag" front="closed" auth user="admin">
<template #description>
Create a new tag in a repository

</template>
<template #request>

```js[Request]
{
    types: [2, 5],
    name: 'My tag',
    short: 'TAG'           // optional
    repository: 'mykmrepo.net',
    problematic: false     // optional
    noLiveDownload: false  // optional
    i18n: {
        eng: 'lol'
    },
    aliases: [],
    priority: 5            // optional
}
```

</template>
<template #success-response>

```js[Response]
{
	code: 200, 
    message: {
        code: 'TAG_CREATED'
        data: <tag object>
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'TAG_CREATE_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get list of years

<endpoint command="getYears" front="restricted" auth user="guest">
<template #description>
Get list of years currently known to the database, with the number of songs in each year
</template>
<template #success-response>

```js[Response]
{
	content: [
        {
            year: 1982,
            karacount: 50 // number of songs for that year
        },
        ...
    ]
    infos: {
        from: 0,
        to: 55, // number of years
        count: 55
    }
}
```


</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'YEARS_LIST_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Get duplicate tags

<endpoint command="getDuplicateTags" front="closed" auth user="admin">
<template #description>
List tags with the same names.T his helps identify when two tags have the same name because they're actually the same but have been created twice by mistake (for example they belong to two different types) or if they're actually two different entities.

Example :

"STUFF" is both a singer and songwriter but has been created twice by mistake. You should merge them.

"SOMEONE" is both a singer and songwriter but actually two different people entirely.

</template>
<template #success-response>

See [Get Tags response](#get-tags)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'TAG_DUPLICATE_LIST_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Merge tags

<endpoint command="mergeTags" front="closed" auth user="admin">
<template #description>
Merge two tags together. The first tag will be kept, the second removed once all its information has been added to the first.
</template>
<template #request>

```js[Request]
{
    tid1: 'abcdef',
    tid2: 'ghijkl'
}
```

</template>
<template #success-response>

```js[Response]
{
  code: 200
  message: {
	code: 'TAGS_MERGED',
    data: <tag object>
  }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'TAGS_MERGED_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Remove tag

<endpoint command="deleteTag" front="closed" auth user="admin">
<template #description>
Remove one or more tags from database and repository
</template>
<template #request>

```js[Request]
{
  tids: ['aaaaaaaaa']
}
```

</template>
<template #success-response>


```js[Response]
{
    code: 200,
    message: {
        code: 'TAG_DELETED',        
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'TAG_DELETE_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get single tag

<endpoint command="getTag" front="closed" auth user="admin">
<template #description>
Get a single tag 
</template>
<template #request>

```js[Request]
{
  tid: 'aaaaaaaaa'  
}
```

</template>
<template #success-response>

See [Get Tags response](#get-tags)

```js[Response]
{
    <tag object>
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'TAG_GET_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Unknown tag">

```js[Response]
{
    code: 404,
    message: {
        code: 'TAG_GET_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400
}
```

</base-code-block>
</template>
</endpoint>

## Edit tag

<endpoint command="editTag" front="closed" auth user="admin">
<template #description>
Edit a tag with new information
</template>
<template #request>

See [Edit Tag request](#create-tag) except you need to add a `tid` property, and all properties are now required.

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'TAG_EDITED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'TAG_EDIT_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Copy tag to another repository

<endpoint command="copyTagToRepo" front="closed" auth user="admin">
<template #description>
Copy a tag to a new repository
</template>
<template #request>

```js[Request]
{
  tid: 'abcdef'
  repo: 'world.karaokes.moe'
}
```

</template>
<template #success-response>

```js[Response]
{
    code: 200
    message: {
        code: 'TAG_COPIED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
    message: {
        code: 'TAG_COPIED_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Get collections

<endpoint command="getCollections" front="closed" auth user="admin">
<template #description>
Get list of collections available. It fetches from all online repositories first, but if some are not reachable, local tags will be used instead.
</template>
<template #success-response>

See [getTags](get-tags) for tag objects.

```js[Response]
[
    <tag object>
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
    message: {
        code: 'COLLECTIONS_GET_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>