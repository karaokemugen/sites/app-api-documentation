---
title: Repositories
description: Repositories management
position: 11
category: Endpoints
---

## Get list of repositories

<endpoint command="getRepos" front="closed" auth user="admin">
<template #description>

Get list of repositories

</template>
<template #success-response>

Note : if the user requesting this is not admin, you'll only get a small subset of each repository, namely `Name`, `Online` and `Enabled` settings. All other settings are admin-only.

```js[Response]
[
    {
        Name: "mykmrepo.net",
        Online: true,
        Enabled: true,
        SendStats: true,
        BaseDir: "app/data",
        AutoMediaDownloads: true,
        MaintainerMode: true,
        Git: {
            URL:           // Git URL for the repository
            Username:      // Git username
            Password:      // Git password
            Author:        // Git identity
            Email:         // Git email
            ProjectID?:    // Gitlab project ID
        };
        FTP: {
            Port:          // FTP port
            Host:          // FTP Host to put medias into
            Username:      // FTP username
            Password:      // FTP Password
            BaseDir:       // Directory inside the FTP where to upload files
        },
   		Path: {
   			Medias: ["app/data/medias"]
   		}
    },
    ...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_LIST_ERROR'
    }
}
```

</template>
</endpoint>

## Add a repository

<endpoint command="addRepo" front="closed" auth user="admin">
<template #description>
Add a new repository
</template>
<template #request>

```js[Request]
{
    Name: 'world.karaokes.moe',
    Online: true // Is it an online repository?
    Enabled: true
    SendStats: true
    Path: {
        Karas: string[]
        Lyrics: string[]
        Medias: string[]
        Tags: string[]
    }
}
```

</template>
<template #success-response>

```js[Response]
{
	code: 200,
    message: {
        code: 'REPO_CREATED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_CREATE_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get a single repository

<endpoint command="getRepo" front="closed" auth user="admin">
<template #description>
Get a single repository
</template>
<template #request>

```js[Request]
{
    name: 'mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Response]
{
        "Name": "mykmrepo.net",
        "Online": true
        "Enabled": true
        "SendStats": true
   		"Path": {
   			"Karas": ["app/data/karaokes"]
   			"Lyrics": ["app/data/lyrics"]
            "Tags": ["app/data/tags"]
            "Medias": ["app/data/medias"]
   		}
},
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GET_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Remove repository

<endpoint command="deleteRepo" front="closed" auth user="admin">
<template #description>
Remove a repository from Karaoke Mugen
</template>
<template #request>

```js[Request]
{
    name: 'mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'REPO_DELETED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_DELETE_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Edit a repository

<endpoint command="editRepo" front="closed" auth user="admin">
<template #description>
Edit a repository
</template>
<template #request>

```js[Request]
{
  name: 'mykmrepo.net'
  newRepo: {
      Name: 'new.mykmrepo.net'
      <See repo object in getRepos>
  }
}
```

</template>
<template #success-response>

```js[Response]
{
  code: 200,
  message: {
      code: 'REPO_EDITED'
  }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_EDIT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Repo not found">

```js[Response]
{
    code: 404
}
```

</base-code-block>
</template>
</endpoint>

## View all unused tags

<endpoint command="getUnusedTags" front="closed" auth user="admin">
<template #description>
Get a list of all tags unused in a repository.
</template>
<template #request>

```js[Request]
{
  name: 'mykmrepo.net'
}
```

</template>
<template #success-response>

Response is made of an array of [Tag objects](tag#tag-object)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GET_UNUSEDTAGS_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## View all unused medias

<endpoint command="getUnusedMedias" front="closed" auth user="admin">
<template #description>
View all unused medias from a repository. Unused medias aren't used by any karaoke.

Since this takes time, this creates a new [Task](index#Tasks).

</template>
<template #request>

```js[Request]
{
  name: 'mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Response]
[
    "/path/to/unused media 1.mp4"
    "/path/to/unused media 2.mp4"
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GET_UNUSEDMEDIA_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Consolidate repository

<endpoint command="consolidateRepository" front="closed" auth user="admin">
<template #description>
This moves all folders from a repository into the same base folder.

Example if you have a repository with:

* Medias: `/path/to/videos`
* Karaokes: `/MyGit/karaokes`
* Lyrics: `/MyGit/lyrics`
* Tags: `/MyGit/tags`

If you consolidate it to `/MyGit/` the videos folder will be moved to `/MyGit/`

</template>
<template #request>

```js[Request]
{
  name: 'mykmrepo.net',
  path: '/MyGit/'
}
```

</template>
<template #success-response>

Consolidation is started and you get an immediate response. It starts a [Task](index#Tasks) so you can check its progress.

```js[Response]
{
    code: 200,
    message: {
        code: 'REPO_CONSOLIDATING_IN_PROGRESS'
    }
}
```

</template>
</endpoint>

## Compare lyrics between repositories

<endpoint command="compareLyricsBetweenRepos" front="closed" auth user="admin">
<template #description>

Get a report of lyrics that differ between two repositories. It is useful when you have two repositories with the same songs in them and wish to make sure lyrics you modified on the first repository are copied to the second one.

</template>
<template #request>

```js[Request]
{
  repo1: 'mykmrepo.net',
  repo2: 'world.karaokes.moe'
}
```

</template>
<template #success-response>

```js[Response]
[
    {
        kara1: <See Kara object>,
        kara2: <See Kara object>
    },
    ...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_COMPARE_LYRICS_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Copy lyrics from a repository to another

<endpoint command="copyLyricsBetweenRepos" front="closed" auth user="admin">
<template #description>
Copies lyrics from one repository to another.
</template>
<template #request>

You feed this command the exact same results you got from [Compare lyrics between repositories](#compare-lyrics-between-repositories)

```js[Request]
[
    {
        kara1: <See Kara object>,
        kara2: <See Kara object>
    },
    ...
]
```

</template>
<template #success-response>

{
    code: 200,
    message: {
        code: 'REPO_LYRICS_COPIED'
    }
}

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_COPY_LYRICS_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Delete all medias from repository

<endpoint command="deleteAllRepoMedias" front="closed" auth user="admin">
<template #description>
Delete all medias from a specific repository (to free up space)
</template>
<template #request>

```js[Request]
{
    name: 'mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'REPO_ALL_MEDIAS_DELETED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_DELETE_ALL_MEDIAS_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Delete old medias from repository

<endpoint command="deleteAllRepoMedias" front="closed" auth user="admin">
<template #description>
Delete old (played more than one month ago) medias from a specific repository (to free up space)
</template>
<template #request>

```js[Request]
{
    name: 'mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'REPO_OLD_MEDIAS_DELETED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_DELETE_OLD_MEDIAS_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Delete Medias

<endpoint command="deleteMedias" front="closed" auth user="admin">
<template #description>
Remove medias by selecting specific songs
</template>
<template #request>

```js[Request]
{
    kids: ['abc', 'def']    
}
```

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'REPO_MEDIAS_DELETED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_DELETE_MEDIAS_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Update all repositories

<endpoint command="updateAllRepositories" front="closed" auth user="admin">
<template #description>
Launches an update of all enabled and online repositories
</template>
<template #success-response>

The update is launched asynchronously so it returns immediately.

</template>
</endpoint>

## Update repository (via Git)

<endpoint command="updateAllRepositories" front="closed" auth user="admin">
<template #description>
Updates a repository using Git (maintainer mode only)
</template>
<template #request>

```js[Request]
{
    repoName: 'mykmrepo.net'
}
```

</template>

<template #success-response>

Simply resolves once the update is done

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_UPDATE_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Stash repository modifications (via Git)

<endpoint command="stashRepo" front="closed" auth user="admin">
<template #description>
Stashes any modifications (via git) in a re pository
</template>
<template #request>

```js[Request]
{
    repoName: 'mykmrepo.net'
}
```

</template>

<template #success-response>

Simply resolves once the update is done

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_STASH_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Check repository status (via Git)

<endpoint command="checkRepo" front="closed" auth user="admin">
<template #description>
Check repository status (clean or not clean).

Clean means no modifications are present adn the repository is updatable.
</template>
<template #request>

```js[Request]
{
    repoName: 'mykmrepo.net'
}
```

</template>

<template #success-response>

Returns a [simple-git status object](https://github.com/steveukx/git-js/blob/c97c08c2630a4318fe65124344180255ee76af51/simple-git/typings/response.d.ts#L285)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_CHECK_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## List repository stashes (via Git)

<endpoint command="listRepoStashes" front="closed" auth user="admin">
<template #description>
Returns a list of repo stashes so that you can choose to pop or drop them
</template>
<template #request>

```js[Request]
{
    repoName: 'mykmrepo.net'
}
```

</template>

<template #success-response>

Returns a [simple-git stash list array](https://github.com/steveukx/git-js/blob/c97c08c2630a4318fe65124344180255ee76af51/simple-git/typings/response.d.ts#L356)

It also contains a `id` property with a number starting from 0 to identify a particular stash, since simple-git doesn't return those.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_CHECK_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Pop repository stash (via Git)

<endpoint command="popStash" front="closed" auth user="admin">
<template #description>
Pops an existing stash into the repository working directory
</template>
<template #request>

```js[Request]
{
    repoName: 'mykmrepo.net',
    stashId: 2
}
```

</template>

<template #success-response>

Simply returns

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_UNSTASH_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Drop repository stash (via Git)

<endpoint command="dropStash" front="closed" auth user="admin">
<template #description>
Removes a stash from the git repository without popping it.
</template>
<template #request>

```js[Request]
{
    repoName: 'mykmrepo.net',
    stashId: 2
}
```

</template>

<template #success-response>

Simply returns

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_UNSTASH_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Reset repository (via Git)

<endpoint command="resetRepo" front="closed" auth user="admin">
<template #description>
Performs a `git reset --hard origin/master` on a repository to reset it to its initial state.
</template>
<template #request>

```js[Request]
{
    repoName: 'mykmrepo.net'    
}
```

</template>

<template #success-response>

Simply returns

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_RESET_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Generate commits to push (via Git)

<endpoint command="getCommits" front="closed" auth user="admin">
<template #description>
Generates a list of commits depending on which files are present in the working directory. The generated list allows you to check which commits are going to be made, modify them (message for example), and then submit them to the `pushCommits` route (see below)

Note: this does **not** create the commits in the git index, but rather generates a list of files to include in each commit. The real action is done by the `pushCommits` route.

</template>
<template #request>

```js[Request]
{
    repoName: 'mykmrepo.net'    
}
```

</template>

<template #success-response>

```js[Response]
{
    commits: [
        {
            addedFiles: ['karaokes/JPN - blabla - OP - truc.kara.json', 'lyrics/JPN - blabla - OP - truc.ass'],
            modifiedFiles: [],
            deletedFiles: [],
            message: 'Add kara'
        }
    ],
    modifiedMedias: [
        {
            old: null,
            new: 'JPN - blabla - OP - truc.mp4',
            commit: 'Add kara'
        }        
    ]
}
```

Note on `modifiedMedias` : 

- If `old` is null (and `new` isn't), this is a new upload
- If `new` is null (and `old` isn't), this is a delete action
- If both `old` and `new` contain the same file, this is a reupload/file change
- If `old` and `new` are different, this is a rename, unless `sizeDifference` exists in the object. In this case, the old file is removed and the new one uploaded as it is a rename + changed file.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_GET_COMMITS_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Push commits and media (via Git)

<endpoint command="pushCommits" front="closed" auth user="admin">
<template #description>
Pushes media via FTP, creates the commits in git and then pushes them, in that order.
</template>
<template #request>

```js[Request]
{
    commits: {
        commits: [
            {
                addedFiles: ['karaokes/JPN - blabla - OP - truc.kara.json', 'lyrics/JPN - blabla - OP - truc.ass'],
                modifiedFiles: [],
                deletedFiles: [],
                message: 'Add kara'
            }
        ],
        modifiedMedias: [
            {
                old: null,
                new: 'JPN - blabla - OP - truc.mp4',
                commit: 'Add kara'
            }        
        ]
    },
    repoName: 'mykmrepo.net',
    ignoreFTP: false //Ignores FTP actions if any should be done.
}
```

</template>

<template #success-response>

Simply returns since this is called asynchronously.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_GIT_PUSH_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Force upload media to FTP

<endpoint command="uploadMedia" front="closed" auth user="admin">
<template #description>
Forces the upload of a song's particular media to the FTP
</template>
<template #request>

```js[Request]
{
    kid: 'abcdefghj'
}
```

</template>

<template #success-response>

Simply returns.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_UPLOAD_MEDIA_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Sync tags between two repositories

<endpoint command="syncTagsBetweenRepos" front="closed" auth user="admin">
<template #description>
Forces tags in `repoDestName` to be updated with their counterparts in `repoSourceName`
</template>
<template #request>

```js[Request]
{
    repoSourceName: mykmrepo.net,
    repoDestName: Local
}
```

</template>

<template #success-response>

Simply returns.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_SYNC_TAGS_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>
