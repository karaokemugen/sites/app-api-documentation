---
title: Sessions
description: Karaoke sessions management
position: 12
category: Endpoints
---

## Get sessions list

<endpoint command="getSessions" front="closed" auth user="admin">
<template #description>

Get list of sessions created

</template>
<template #success-response>

```js[Response]
[
	{
        "active": true,
 	    "name": "Jonetsu IV Day 1",
 		"seid": "..."
 		"started_at": "Sat 13 Oct 2019 09:30:00",
 		"private": true, // Private sessions won't be sent to KM Server
 		"ended_at": "Sat 13 Oct 2019 18:00:00"
 	},
 	...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SESSION_LIST_ERROR'
    }
}
```

</template>
</endpoint>

## Create session

<endpoint command="createSession" front="closed" auth user="admin">
<template #description>
Add a new session to the list
</template>
<template #request>

```js[Request]
{
    name: 'Jonetsu IV Day 2', // This one is mandatory
    started_at: '2019-04-05 09:30',
    ended_at: null,
    activate: true, // Make this the active session immediately
    private: false, // Session is not private and will be sent to KM Server for analysis
}
```

</template>
<template #success-response>

```js[Response]
{
	code: 200,
    message: {
        code: 'SESSION_CREATED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SESSION_CREATION_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Merge sessions

<endpoint command="mergeSessions" front="closed" auth user="admin">
<template #description>
Merge two sessions into a new one. Data from both sessions will be aggregated into a new one. The two sessions are then removed.
</template>
<template #request>

```js[Request]
{
    seid1: <uuid>,
    seid2: <uuid>
}
```

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'SESSIONS_MERGED'
        data: {
            session: { <new session object> }
        }
    }
},
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SESSION_MERGE_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Edit session

<endpoint command="editSession" front="closed" auth user="admin">
<template #description>
Edit session
</template>
<template #request>

See [Get Sessions List](#get-sessions-list).

```js[Request]
{
    <session object>
}
```

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'SESSION_EDITED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SESSION_EDIT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'ERROR_CODES.SESSION_NOT_FOUND'
    }
}
```

</base-code-block>
<base-code-block label="Wrong dates">

Sent when a session ends before it starts, or vice versa.

```js[Response]
{
    code: 409,
    message: {
        code: 'ERROR_CODES.SESSION_END_BEFORE_START_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Activate session

<endpoint command="activateSession" front="closed" auth user="admin">
<template #description>
Session will be made active : all played and requested songs will be linked to that session from now on.
</template>
<template #request>

```js[Request]
{
  seid: <uuid>
}
```

</template>
<template #success-response>

```js[Response]
{
  code: 200,
  message: {
      code: 'SESSION_ACTIVATED'
  }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REPO_EDIT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400   
}
```

</base-code-block>
</template>
</endpoint>

## Remove session

<endpoint command="deleteSession" front="closed" auth user="admin">
<template #description>
Delete session from database
</template>
<template #request>

```js[Request]
{
  seid: <uuid>
}
```

</template>
<template #success-response>

```js[Response]
{
  code: 200,
  message: {
      code: 'SESSION_DELETED'
  }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SESSION_DELETE_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,    
}
```

</base-code-block>
<base-code-block label="Forbidden">

You cannot remove active sessions

```js[Response]
{
    code: 403,
    message: {
        code: 'SESSION_DELETE_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'SESSION_DELETE_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Export session

<endpoint command="exportSession" front="closed" auth user="admin">
<template #description>
Export session data (played and requested songs, as well as top requested and number of played songs) in CSV format.
</template>
<template #request>

```js[Request]
{
  seid: <uuid>
}
```

</template>
<template #success-response>

Response contains names of files for each report. You can give access to them through `http://localhost:<port>/sessionExports/<filename>`

```js[Response]
[
    requested: "My Session.2021-04-06.requested.csv",
    played: "My Session.2021-04-06.played.csv",
    playedCount: "My Session.2021-04-06.playedCount.csv",
    requestedCount: "My Session.2021-04-06.requestedCount.csv"
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SESSION_EXPORT_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>