---
title: Inbox
description: Manage a KM Server inbox
position: 20
category: Endpoints
---

These endpoints have to be used with a working Karaoke Mugen Server instance. It allows the client to query the inbox of songs submitted by other users, download them and delete them once the song has been added to database.

## Get inbox

<endpoint command="getInbox" front="closed" auth user="admin">
<template #description>

Returns inbox content after successfully connecting to the server

</template>
<template #request>

Your request needs to have a valid `onlineAuthorization` header from previously logging in with a valid online user.

```js[Request]
{
    repoName: 'mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Response]
[
	{
		inid: <inbox item id>
		name: 'ENG - My little series - OP - My little song'
		username_downloaded?: 'Aeden' //Who downloaded the song last
		downloaded_at?: <Date>;
		created_at: <Date>;
		kara: {
			file: 'ENG - My little series - OP - My little song.kara.json',
			data: <KaraFile data in JSON>
		};
		lyrics: {
			file: 'ENG - My little series - OP - My little song.ass',
			data: <File contents as a string>
		};
		extra_tags: [] // If any new tags are included with this song, they'll be listed there, with file and data properties
		mediafile: 'ENG - My little series - OP - My little song.mp4'
		gitlab_issue: '2098'
		fix: false // true if this is an edit/fix of an existing song, false if brand new
		contact: ''
	},
	...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'INBOX_VIEW_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Download song from inbox

<endpoint command="downloadKaraFromInbox" front="closed" auth user="admin">
<template #description>
Download and integrate song from inbox into the app's database.
</template>

<template #request>

Your request needs to have a valid `onlineAuthorization` header from previously logging in with a valid online user.

```js[Request]
{
    repoName: 'mykmrepo.net',
	inid: 'abcdefghijkl' // Inbox Item ID
}
```

</template>


<template #success-response>

Simply returns.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'DOWNLOAD_KARA_FROM_INBOX_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>

## Delete song from inbox

<endpoint command="downloadKaraFromInbox" front="closed" auth user="admin">
<template #description>
Deletes a song once it's been integrated successfully in the main database by a maintainer.
</template>

<template #request>

Your request needs to have a valid `onlineAuthorization` header from previously logging in with a valid online user.

```js[Request]
{
    repoName: 'mykmrepo.net',
	inid: 'abcdefghijkl' // Inbox Item ID
}
```

</template>

<template #success-response>

Simply returns.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'DELETE_KARA_IN_INBOX_ERROR'
    }
}
```

</base-code-block>
</template>

</endpoint>
