---
title: Karaokes
description: Get, add, remove songs
position: 6
category: Endpoints
---

## Tag types

See [the tagType object from the shared lib](https://gitlab.com/karaokemugen/lib/-/blob/master/utils/constants.ts#L59). Each type is self-explanatory and has a number ID.

## The `q` parameter

When requesting songs, you'll be able to send a `q` property with text allowing you to better query the list of songs.

`q` is a text property in the request body. Its format is as follows:

- `!` is a separator between criterias
- The different criterias are :
  - `y:` for year. Example: `y:1982`. Multiple years can be selected, for example `y:1982,1983,1984,1985`
  - `r:` for repository. Example: `r:mykmrepo.net`
  - `t:` for tag and type in the following format `<tag ID>~<type>`. Example: `abcdef~2` means we're looking for songs with the tag `abcdef` as singer. See tag types above. You can request several tags at once by separating them by `,`. For example `t:abcdef~2,ghijkl~8`. This will show all songs containing *all* those tags.
  - `at:` see `t:` above, except songs containing *any* tags from this list will show up.
  - `k:` for specific songs. If you want to fetch a single song, or two, use `k:abcdef` where `abcdef` is your karaoke UUID. You can specify several songs by separating them by `,` just like tags above.
  - `m:` for media presence in local. `m:DOWNLOADED` will show all downlaoded songs. Other values can be `MISSING` or `DOWNLOADING` even.
  - `eid:` for external database IDs. `eid:anilist,2066` will show songs from series 2066 on Anilist. Database services allowed are `anilist`, `myanimelist` and `kitsu`.
  - `seid:` to get songs from a specific karaoke session. See the [sessions API](sessions) for more information.

## Get songs

<endpoint command="getKaras" front="restricted" auth user="guest">
<template #description>

Query list of songs from the database.

</template>
<template #request>

```js[Request]
{
  filter: 'Nana Mizuki',   // simple text filter
	from: 0,
	size: 400,
	order: 'favorited',
	q: <see the q request at the top of the document>,
	random: 2                // returns 2 random songs among the query results,
  blacklist: true          // hide blacklisted songs,
  ignoreCollections: false // Ignore collection system settings.
}
```

A note on Collections :

Collections are a special tag type which is defined system-wide by the karaoke operator. It allows to select which types of songs to show (or not) to the public. Contrary to a blacklist, songs in collections *not* included in the enabled collections system setting won't be displayed either to the operator. You can use the `ignoreCollections` parameter to get a full list of songs in database, wether their collection is enabled or not.

`order` can be :

- `favorited` : user's favorites. The user is decided by its authorization token
- `sessionRequested` : list by number of requests in a specific session.
- `sessionPlayed` : list by number of times the song has been played in a specific session
- `requested` : list by number of requests among all sessions, or among online stats data if enabled in config (default)
- `requestedLocal` : same as `requested` but it forces songs to be fetched from local database
- `history` : list songs by last date played in descending order (most recent first)
- `recent` : list by most recent creation date (to get all the most recent songs)
- `played` : list by number of times the song has been played on your Karaoke Mugen app.

</template>
<template #success-response>

This return an object with several important properties.

- `infos` contains information about the results. Number of total results, from and to which (for pagination)
- `i18n` is the i18n data for every tag in the request. For example if three songs use the same tag, the i18n data for that tag will be listed here once instead of three times, to save on bandwidth data.
  - Not all tags have a translation! If the `eng` translation is only one AND the same as the tag name, it won't be listed in the `i18n` object.
- `avatars` is not used for song lists. See [playlist API](playlist#get-playlist-content) as it's used here to display avatar file info for each user
- `content` contains a karaoke object. See example response for details
  - Each song has tags by types. Each type is an array of tags. See [tag types](#tag-types)
  - If a song has a tag with a `problematic` property set to true, you should display a warning next to the song. Problematic tags are usually Adults Only, Epilpsy or Spoilers.
  - `download_status` refers to if the song's media is present on disk or not. Missing media means the song is "in the cloud".

```js[Response]
{
    "i18n": {
      "07474742-fb4f-46d6-9140-1545b637f33c": {
        "eng": "THE iDOLM@STER Million Live!: Theater Days",
        "jpn": "アイドルマスターミリオンライブ!シアターデイズ"
      },
      "7be1b15c-cff8-4b37-a649-5c90f3d569a9": {
        "eng": "Music Video"
      },
      "4dcf9614-7914-42aa-99f4-dbce2e059133": {
        "afr": "Japanees",
        "alb": "Japoneze",
        "amh": "ጃፓንኛ",
        "ara": "اليابانية",
        "arm": "Ճապոներեն",
        "aze": "Yapon",
        "bak": "Япон теле",
        "baq": "Japoniako",
        "bel": "Японскі",
        "ben": "জাপানি",
        "bos": "Japanci",
        "bul": "Японски",
        "cat": "Japonès",
        "chi": "日本",
        "cze": "Japonské",
        "dan": "Japansk",
        "dut": "Japans",
        "eng": "Japanese",
        "epo": "Japana",
        "est": "Jaapani",
        "fin": "Japanilainen",
        "fre": "Japonais",
        "geo": "იაპონელი",
        "ger": "Japanisch",
        "gla": "Italian",
        "gle": "Seapáinis",
        "glg": "Xaponés",
        "gre": "Ιαπωνικά",
        "guj": "જાપાનીઝ",
        "hat": "Japonè",
        "heb": "יפנית",
        "hin": "जापानी",
        "hrv": "Japanski",
        "hun": "Japán",
        "ice": "Japanska",
        "ind": "Jepang",
        "ita": "Giapponese",
        "jav": "Jepang",
        "jpn": "日本語",
        "kan": "ಜಪಾನೀಸ್",
        "kaz": "Жапон",
        "kir": "Японский",
        "kor": "일본어",
        "lat": "Facilisis",
        "lav": "Japāņu",
        "lit": "Japonų",
        "mac": "Јапонски",
        "mal": "ജാപ്പനീസ്",
        "mao": "Hapanihi",
        "mar": "जपानी",
        "may": "Jepun",
        "mlg": "Japoney",
        "mlt": "Ġappuniż",
        "mon": "Японы",
        "nep": "जापानी",
        "nor": "Japansk",
        "pan": "ਜਪਾਨੀ",
        "per": "ژاپنی",
        "pol": "Japoński",
        "por": "Japonês",
        "rum": "Japoneze",
        "rus": "Японский",
        "sin": "ජපන්",
        "slo": "Japonský",
        "slv": "Japonski",
        "spa": "Japonés",
        "srp": "Јапански",
        "sun": "Jepang",
        "swa": "Kijapani",
        "swe": "Japanska",
        "tam": "ஜப்பனீஸ்",
        "tat": "Япон теле",
        "tel": "జపనీస్",
        "tgk": "Ҷопон",
        "tgl": "Hapon",
        "tha": "ภาษาญี่ปุ่น",
        "tur": "Japon",
        "ukr": "Японський",
        "urd": "جاپانی",
        "uzb": "Yaponiya",
        "vie": "Nhật bản",
        "wel": "Siapan",
        "xho": "Isijapanese",
        "yid": "יאַפּאַניש"
      },
      "dbedd6b3-d125-4cd8-aa32-c4175e4ca3a3": {
        "eng": "Video Game",
        "fre": "Jeu Vidéo"
      },
      "213a1a15-e73a-4a08-b3f6-b6e2745a82e5": {
        "eng": "Mobile Game",
        "fre": "Mobage"
      }
    },
    "avatars": {},
    "content": [
      {
        "kid": "94be430f-6240-41e4-883d-c95b550cdf13",
        "titles": {
          "eng": "Last Actress"
        },
        "titles_aliases": [],
        "songorder": null, // number here if not null. OP3, ED2, etc.
        "subfile": "JPN - THE iDOLM@STER Million Live! Theater Days - GAME MV - Last Actress.ass",
        "singers": [
          {
            "tid": "27204220-fdf3-4aff-a78d-ad3fb9ffe8ff",
            "name": "Minami Takahashi",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          },
          {
            "tid": "8dcd8b3f-3dfd-44e9-ac7b-5568f4834c4c",
            "name": "Keiko Watanabe",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          },
          {
            "tid": "b8f8a8a2-c9cb-403f-88ab-269798fd3511",
            "name": "Rika Abe",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          },
          {
            "tid": "d9cad1b8-35dc-449c-a2a9-66be5c0ddbd4",
            "name": "Saki Minami",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          },
          {
            "tid": "f335316b-fad2-44bc-bc41-717f11ed454e",
            "name": "Risa Taneda",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "songtypes": [
          {
            "tid": "7be1b15c-cff8-4b37-a649-5c90f3d569a9",
            "name": "MV",
            "short": null,
            "priority": 15,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "creators": [
          {
            "tid": "c3e622d4-2686-4331-8691-d528c8647088",
            "name": "BANDAI NAMCO Entertainment",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "songwriters": [
          {
            "tid": "1900b35d-bd27-4a89-9b27-6e3e2af731fc",
            "name": "Shûhei Mutsuki",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          },
          {
            "tid": "ead196cc-e1e3-43a4-8804-dd4199e89c4e",
            "name": "Kanata Nakamura",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          },
          {
            "tid": "ed388426-40c8-4900-afe5-2d816cc67541",
            "name": "R.O.N",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "year": 2018,
        "langs": [
          {
            "tid": "4dcf9614-7914-42aa-99f4-dbce2e059133",
            "name": "jpn",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "authors": [
          {
            "tid": "06852e02-b77e-4106-b6f7-2d03942ad0ea",
            "name": "meulahke",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "groups": [
          {
            "tid": "ae613721-1fbe-480d-ba6b-d6d0702b184d",
            "name": "2010s",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "misc": [],
        "origins": [],
        "platforms": [
          {
            "tid": "213a1a15-e73a-4a08-b3f6-b6e2745a82e5",
            "name": "Mobage",
            "short": "MOB",
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "families": [
          {
            "tid": "dbedd6b3-d125-4cd8-aa32-c4175e4ca3a3",
            "name": "Video Game",
            "short": "VG",
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "genres": [
          {
            "tid": "caa86df2-0d59-474b-885c-f240a9e891b0",
            "name": "Idol",
            "short": "IDL",
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "series": [
          {
            "tid": "07474742-fb4f-46d6-9140-1545b637f33c",
            "name": "THE iDOLM@STER Million Live!: Theater Days",
            "short": null,
            "priority": 10,
            "problematic": false,
            "nolivedownload": false,
            "repository": "mykmrepo.net"
          }
        ],
        "warnings": [],
        "versions": [],
        "mediafile": "JPN - THE iDOLM@STER Million Live! Theater Days - GAME MV - Last Actress.mp4",
        "karafile": "JPN - THE iDOLM@STER Million Live! Theater Days - GAME MV - Last Actress.kara.json",
        "duration": 149, //in seconds
        "gain": -10.79, //replaygain data for mediaplayer use
        "loudnorm": "-6.93,7.75,3.20,-17.01,-0.70", //loudnorm data for mediaplayer use
        "created_at": "2020-07-25T01:28:02.835Z",
        "modified_at": "2021-02-25T13:22:02.928Z",
        "mediasize": 186320034,
        "played": 0, // # times the song has been played
        "requested": 0, // ...and requested
        "lastrequested_at": null, // date at which the song was last requested
        "flag_dejavu": false, // has the song been played recently? configurable
        "lastplayed_at": "2021-03-16T20:23:35.028Z",
        "lastplayed_ago": {
          "hours": 14,
          "minutes": 41,
          "seconds": 2,
          "milliseconds": 369.57
        },
        "flag_favorites": false, //is the song a favorite of the user?
        "repository": "mykmrepo.net",
        "tid": [ // list of tag IDs with types used in song.
          "06852e02-b77e-4106-b6f7-2d03942ad0ea~6",
          "c3e622d4-2686-4331-8691-d528c8647088~4",
          "dbedd6b3-d125-4cd8-aa32-c4175e4ca3a3~10",
          "caa86df2-0d59-474b-885c-f240a9e891b0~12",
          "ae613721-1fbe-480d-ba6b-d6d0702b184d~9",
          "4dcf9614-7914-42aa-99f4-dbce2e059133~5",
          "213a1a15-e73a-4a08-b3f6-b6e2745a82e5~13",
          "07474742-fb4f-46d6-9140-1545b637f33c~1",
          "27204220-fdf3-4aff-a78d-ad3fb9ffe8ff~2",
          "8dcd8b3f-3dfd-44e9-ac7b-5568f4834c4c~2",
          "b8f8a8a2-c9cb-403f-88ab-269798fd3511~2",
          "d9cad1b8-35dc-449c-a2a9-66be5c0ddbd4~2",
          "f335316b-fad2-44bc-bc41-717f11ed454e~2",
          "7be1b15c-cff8-4b37-a649-5c90f3d569a9~3",
          "1900b35d-bd27-4a89-9b27-6e3e2af731fc~8",
          "ead196cc-e1e3-43a4-8804-dd4199e89c4e~8",
          "ed388426-40c8-4900-afe5-2d816cc67541~8"
        ],
        "public_plc_id": [], // PLC IDs the song has in the public playlist
        "flag_upvoted": false, // has it been upvoted by the user?
        "my_public_plc_id": [] // PLC IDs the song has in the public if they've been added by the user,
        "download_status": "DOWNLOADED" // can be MISSING or DOWNLOADING.
        "comment": "My first karaoke!" // Karaoke author comment,
        "ignoreHooks": false // Karaoke does not ignore tag hooks
        "parents": ["ed388426-40c8-4900-afe5-2d816cc67541"],
        "children": [],
        "siblings": []
      }
    ],
    infos: {
        count: 201,
        from: 0,
        to: 201
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SONG_LIST_ERROR'
    }
}
```

</template>
</endpoint>

## Create song

<endpoint command="createKara" front="closed" auth user="admin">
<template #description>
Create a new song in the database and write data to files in selected repository.

</template>
<template #request>

```js[Request]
{
    kara: <KaraFileV4>     // See below
}
```

Before sending this request, you need to create new tags, if any, and send your lyrics and media files through the REST API `/api/importfile`. See [uploading files](index#uploading-files). For each file, you'll get a `filename` property which you need to pass to `mediafile` and `lyricsfile` (if applicable)

Once uploaded, you need to run your media through the [processUploadedMedia](#process-uploaded-media-file) command. This will return all the info you need on your media 

Notes:

- `lyrics` can be empty if the video you're uploading contains hardsubs or embedded lyrics (like in a MKV file)
- `series` OR `singers` are mandatory. A song must have either a series or singer.
- `collections`, `songtypes`, `authors` and `langs` are mandatory

A KaraFileV4 JSON document is expected in the body of your request. You can see [what the type is like here](https://gitlab.com/karaokemugen/lib/-/blob/master/types/kara.d.ts#L102) or if you want a full example, check out any `kara.json` file in [the main karaoke base repository](https://gitlab.com/karaokemugen/bases/karaokebase)

</template>
<template #success-response>

```js[Response]
{
	code: 200,
  message: {
    code: 'KARA_CREATED',
  }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'KARA_CREATED_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Remove song

<endpoint command="deleteKaras" front="closed" auth user="admin">
<template #description>
Remove songs from database and delete its files
</template>
<template #request>

```js[Request]
{
    kids: ['2fed9947-a1b4-4a36-b8da-0e417d2b8ee6']
}
```

</template>
<template #success-response>

```js[Response]
{
	code: 200,
  message: {
    code: 'KARA_DELETED'
  }
}
```


</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'KARA_DELETED_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Kara not found">

```js[Response]
{
    code: 404,
    message: {
        code: 'KARA_DELETED_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Add song(s) to public playlist

<endpoint command="addKaraToPublicPlaylist" front="open" auth user="guest">
<template #description>
Adds songs to the public playlist. This is typically the route used by public users, not operators, to add songs. A lot more checks are made through this route (like checking against the user's quota, etc.)
</template>
<template #request>

```js[Request]
{
    kids: ['2fed9947-a1b4-4a36-b8da-0e417d2b8ee6']
}
```

</template>
<template #success-response>

```js[Response]
{
  code: 'PL_SONG_ADDED'
  data: {
	action: 'ADDED',
	kara: 'My super song',
	playlist: 'Public playlist',
	kid: ['2fed9947-a1b4-4a36-b8da-0e417d2b8ee6'],
	plaid: 1,
	plc: <...>
  }
}
```

For `plc` see [playlist contents response](playlists#get-playlist-contents)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PLAYLIST_MODE_ADD_SONG_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Unknown song">

```js[Response]
{
    code: 404,
    message: {
        code: 'PLAYLIST_MODE_ADD_SONG_ERROR',
		data: 'One of the karaokes does not exist'
    }
}
```
</base-code-block>
<base-code-block label="Song blacklisted">

```js[Response]
{
    code: 451,
    message: {
        code: 'PLAYLIST_MODE_ADD_SONG_ERROR_BLACKLISTED'
    }
}
```
</base-code-block>
<base-code-block label="Out of quota">

User has no more song quota

```js[Response]
{
    code: 429,
    message: {
        code: 'PLAYLIST_MODE_ADD_SONG_ERROR_QUOTA_REACHED'
    }
}
```
</base-code-block>
<base-code-block label="No dupe series/singer allowed">

Config is set to disallow adding more than one song from the same series or singer

```js[Response]
{
    code: 406,
    message: {
        code: 'PLAYLIST_MODE_ADD_SONG_ERROR_NO_DUPLICATE_SERIES_SINGERS'
    }
}
```
</base-code-block>
<base-code-block label="Song already added">

```js[Response]
{
    code: 409,
    message: {
        code: 'PLAYLIST_MODE_ADD_SONG_ERROR_ALREADY_ADDED'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Edit song

<endpoint command="editKara" front="closed" auth user="admin">
<template #description>
Edit a song in database and in its files.
</template>
<template #request>

Remember to upload and process a media file, or create new tags if you need to before editing a song. Refer to [the kara creation command](#create-song) for more details, and set the two booleans accordingly:

```js[Request]
{
    kara: <KaraFileV4>     // See createKara command
    modifiedLyrics: false, 
    modifiedMedia: true,  
}
```

</template>

<template #success-response>

```js[Response]
{
  code: 200
  message: {
	  code: 'KARA_EDITED'
  }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'KARA_EDITED_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get song lyrics

<endpoint command="getKaraLyrics" front="restricted" auth user="guest">
<template #description>
Get a song lyrics.
</template>
<template #request>

```js[Request]
{
  kid: 'aaaaaaaaa'
}
```

</template>
<template #success-response>

You'll get nothing if the song is a hardsub / has absolutely no lyrics. Else :

```js[Response]
[
    {
      "start": 48.6, // This is in seconds
      "end": 54.91,
      "text": "sora tobu hane to hikikae ni"
    },
    {
      "start": 55.1,
      "end": 61.38,
      "text": "tsunagiau te wo eranda bokura"
    },
    ...
]

```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'LYRICS_VIEW_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Copy song to another repository

<endpoint command="copyKaraToRepo" front="closed" auth user="admin">
<template #description>
Copy a song to another repository, modifying its `repository` property. It also copies necessary tags.
</template>
<template #request>

```js[Request]
{
  kid: 'aaaaaaaaa',
  repo: 'world.karaokes.moe'
}
```

</template>
<template #success-response>

```js[Response]
{
	code: 200,
  message: {
    code: 'SONG_COPIED'
  }
}

```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SONG_COPIED_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400
}
```

</base-code-block>
</template>
</endpoint>

## Play song from library

<endpoint command="playKara" front="closed" auth user="admin">
<template #description>
Play a song directly from the library, without a need for it to be in a playlist.
</template>
<template #request>

```js[Request]
{
  kid: 'aaaaaaaaa'
}
```

</template>
<template #success-response>

No response, the request returns once play has started.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'SONG_PLAY_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Edit songs in batch

<endpoint command="editKaras" front="closed" auth user="admin">
<template #description>
Add or remove a tag to/from several songs at once.
</template>
<template #request>

Before sending the request, you need to create a playlist with the songs you wish to batch-edit.

```js[Request]
{
  plaid: 5,
  action: 'add',
  tid: 'bbbbbbbbb',
  type: 2 //tag type
}
```

</template>
<template #success-response>

No response, the request returns immediately as this process can take time. It's started immediately and asynchronously and can be followed via a [Task](index#tasks).

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500
}
```

</base-code-block>
</template>
</endpoint>

## Remove media file

<endpoint command="deleteMediaFile" front="closed" auth user="admin">
<template #description>
Remove only the media of a song (to free up space)

This will try to find the media in all folders for the repository and delete the first one.

This is usually for unused medias list. To delete medias from specific songs, use [the repository's `deleteMedias` API](repo#deleteMedias)

</template>
<template #request>

```js[Request]
{
    file: 'My song.mp4',
    repo: 'mykmrepo.net'
}
```

</template>
<template #success-response>

Simply returns

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'MEDIA_DELETE_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Open lyrics file

<endpoint command="openLyricsFile" front="closed" auth user="admin">
<template #description>
Opens a lyrics file on your OS. 

</template>
<template #request>

Supply a Karaoke ID.

```js[Request]
{
    kid: 'abcdefghijkl'
}
```

</template>
<template #success-response>

Simply returns

</template>
</endpoint>

## Open media in folder

<endpoint command="openMediaFolder" front="closed" auth user="admin">
<template #description>
Opens the repository's media folder

</template>
<template #request>

Supply a repository name

```js[Request]
{
  name: "mykmrepo.net"
}
```

</template>
<template #success-response>

Simply returns

</template>
</endpoint>


## Process uploaded media file

<endpoint command="processUploadedMedia" front="closed" auth user="admin">
<template #description>
Processes a previously uploaded media file (via the send file API) to get all its technical data like loudnorm, audio gain, file size, etc. This also weboptimizes the file if needed.

The resulting response should be used to fill the kara object you send to create a karaoke.

</template>
<template #request>

`filename` is returned by the send file API called previously. `origFilename` is the original name on the user's computer.

```js[Request]
{
  filename: 'abcdefghjkllkj',
  origFilename: 'My new karaoke.mkv'
}
```

</template>
<template #success-response>

```js[Response]
{
  size: 12910239,
  filename: "processed_zeldajzedlj.mp4"  ,
  error: false,
  gain: -1.20,
  loudnorm: "...",
  duration: 90,
  mediaType: 'video' // audio or video
  overallBitrate: 8000 // in kbps
  videoCodec: 'av1'
  videoColorspace: 'yuv720p'
  videoAspectRatio: {
    pixelAspectRatio: '5:1' // PAR / SAR (on ffmpeg)
    displayAspectRatio: '1:3' // DAR
  };
  audioCodec: 'aac';
  audioSampleRate: 44100;
  videoResolution: { 
    height: 1920,
    width: 1080,
    formatted: '1920x1080'
  },
  videoFramerate: 25,
  hasCoverArt: false, // for Audio only    
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'UPLOADED_MEDIA_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Preview hooks

<endpoint command="previewHooks" front="closed" auth user="admin">
<template #description>
Takes a kara and lists all hooks it'll trigger before applying them, so you can preview what they'll do before submitting the kara as a new/modified song

</template>
<template #request>

Send a `KaraFileV4` object.

</template>
<template #success-response>

Receive a `Tag` array

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'PREVIEW_HOOKS_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Get media info

<endpoint command="getKaraMediaInfo" front="closed" auth user="admin">
<template #description>
Returns video and audio codec information from a media file
</template>
<template #request>

Replace `<kid>` by the KID of your choice

```js[Request]
{
  kid: '<kid>'  
}
```

</template>
<template #success-response>

```js[Response]
{
  error: false,
  size: 123910239,
  gain: -1.20,
  duration: 90,
  loudnorm: "...",
  filename: "ENG - Suzume - AMV - Another Axel AMV.mp4",
  fileExtension: "mp4",
  overallBitrate: 8902901
  videoCodec: "h264",
  audioCodec: "aac",
  videoResolution: "1280x720",
  videoColorspace: "rgb"
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'GET_MEDIA_INFO_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Validate media info

<endpoint command="validateMediaInfo" front="closed" auth user="admin">
<template #description>
Verifies that the provided media information is valid as per the repository rules.
</template>
<template #request>

```js[Request]
{
  mediaInfo: {
    size: 12345678
    filename: 'abcedfgh.mp4' // File in temp folder
    fileExtension: 'mp4'
    loudnorm: "-12,3.5" // Loudnorm string
    duration: 120 // in seconds
    mediaType: 'video' // audio or video
    overallBitrate: 8000 // in kbps
    videoCodec: 'av1'
    videoColorspace: 'yuv720p'
    videoAspectRatio: {
      pixelAspectRatio: '5:1' // PAR / SAR (on ffmpeg)
      displayAspectRatio: '1:3' // DAR
    };
    audioCodec: 'aac';
    audioSampleRate: 44100;
    videoResolution: { 
      height: 1920,
      width: 1080,
      formatted: '1920x1080'
    },
    videoFramerate: 25,
    hasCoverArt: false, // for Audio only    
  },
  repository: 'mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Response]
[
  {
    name: 'overallBitrate', // One of mediaInfo keys
    mandatory: false, // if false, just a warning
    suggestedValue: 7000,
    resolvableByTranscoding: true 
  }
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 406,
    message: {
        code: 'MEDIA_VALIDATION_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

