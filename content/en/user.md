---
title: Users
description: Many user-related functions
position: 15
category: Endpoints
---

## Security code

A six-digit security code is generated at the app's startup sequence. It can be found in logs. 

This code is used in special cases where you're not logged in as operator but need operator privileges, like creating a new operator user for example.

## User names

if a user name contains a `@` character, it's considered an online account. If not, it is a local account.

The rightmost part of a username with a `@` is the server it is attached to.

## User types

- 0 : Operators (also called admins)
- 1 : Regular user
- 2 : Guest

Guests have limited access (they can't have favorites).

## Online users

Users are considered online if they have a `@` in their name.

For edit routes you'll need to provide your `onlineAuthorization` token along with the `authorization` one.

## Language preferences

Users can have two languages settings. Each uses [a ISO639-2B code](https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes) : 

- `main_series_lang` : Main language to use
- `fallback_series_lang` : Language to use if the main one isn't available on a particular series.

NOTE: the property name has "series" in it for legacy reasons, and is not specific to series.

If all else fails, the default title/name will be used (specific to each song/tag).

## Get all users

<endpoint command="getUsers" front="restricted" auth user="guest">
<template #description>

List all users in database. If listed by a non-admin, even users with flag_public at false are listed.


</template>
<template #success-response>

```js[Response]
[
    {
        type: 0,
        avatar_file: 'abcdefghijkl.png',
        login: 'axel@mykmrepo.net',
        nickname: 'AxelTerizaki',
        last_login_at: 2021-05-04 12:09:11
        flag_online: true  // True if the user has been using the frontend recently (15 minutes ago at least by default)
    },
    ...
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_LIST_ERROR'
    }
}
```

</template>
</endpoint>

## Create user

<endpoint command="createUser" front="restricted">
<template #description>
Create a new user.

</template>
<template #request>

Auth is optional. In order to create an operator (`admin`) account, you need either the [security code](#security-code) or an auth token belonging to a operator user.

You don't need either to create regular user accounts.

```js[Request]
{
    login: 'axel@mykmrepo.net',
    password: 'blablabla',
    role: 'admin' // user or admin
    securityCode: 123456 // optional
}
```

</template>
<template #success-response>

```js[Response]
{
	code: 200, 
    message: {
        code: 'USER_CREATED'        
    }
}
```

</template>
<template #errors>
<base-code-block label="Unauthorized">

Not enough privileges to create an admin account or wrong security code.

```js[Response]
{
    code: 403,
    message: {
        code: 'UNAUTHORIZED'
    }
}
```

</base-code-block>
<base-code-block label="No online admin allowed">

You cannot create online accounts with the name `admin`

```js[Response]
{
    code: 403,
    message: {
        code: 'USER_CREATE_ERROR',
        data: 'Admin accounts are not allowed to be created online'
    }
}
```

</base-code-block>
<base-code-block label="Online creation disabled">

Admin has disabled online account creation

```js[Response]
{
    code: 403,
    message: {
        code: 'USER_CREATE_ERROR',
        data: 'Creating online accounts is not allowed on this instance'
    }
}
```

</base-code-block>
<base-code-block label="Password too short">

```js[Response]
{
    code: 411,
    message: {
        code: 'PASSWORD_TOO_SHORT'        
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get single user

<endpoint command="getUser" front="restricted" auth user="guest">
<template #description>
Get full user profile. If listed by a non-admin, even users with flag_public at false are listed.


</template>
<template #request>

```js[Request]
{
    username: 'axel@mykmrepo.net'
}
```

</template>

<template #success-response>

If the user making the request is an admin, `bio`, `email` and `url` will be filled. If not, it'll be null.

```js[Response]
{
	type: 0,
    login: 'axel@mykmrepo.net',
    nickname: 'AxelTerizaki',
    password: null,
    bio: null,
    email: null,
    url: null,
    avatar_file: 'abcdefgh.png',
    last_login_at: 2021-05-04 12:09:11,
    flag_online: true,
    main_series_lang: 'fre',
    fallback_series_lang: 'eng',
    flag_tutorial_done: true,    // has user completed the admin tutorial?
    language: 'fre', // User language
    flag_parentsonly: true, // User only wants to see parents in karaoke lists
    flag_public: true, // User profile is publicly visible,
    flag_displayfavorites: true, // User allows its favorites to be seen by others
    social_networks: {
        discord: 'something#1234',
        twitter: '@something',
        twitch: 'somethingTV',
        instagram: 'something'
    },
    banner: 'abdecfgh.png',
    anime_list_to_fetch: 'myanimelist', // or "anilist" or "kitsu"
    anime_list_last_modified_at: <Date>,
    anime_list_list_ids: [202, 203, 205, ...] // ids of series the user has seen on the anime list provider
}
```



</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_VIEW_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Unknown user">

```js[Response]
{
    code: 404,
    message: {
        code: 'USER_VIEW_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Delete user (locally)

<endpoint command="deleteUser" front="closed" auth user="admin">
<template #description>
Delete a user. This is an admin-only route used in the system panel.

Regular users can delete their own accounts with another route.

</template>
<template #request>

```js[Request]
{
    username: 'axel@mykmrepo.net'
}
```

</template>
<template #success-response>

```js[Request]
{
    code: 200,
    message: {
        code: 'USER_DELETED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Deleting admin is forbidden">

```js[Response]
{
    code: 406,
    message: {
        code: 'USER_DELETE_ADMIN_DAMEDESU'
    }
}
```

</base-code-block>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_DELETE_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="User unknown">

```js[Response]
{
    code: 404,
    message: {
        code: 'USER_NOT_EXISTS'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Edit user (locally)

<endpoint command="editUser" front="closed" auth user="admin">
<template #description>
Edit a user (from the system panel). 

If it's an online user, only editing its type and flag_tutorial_done is permitted. 

If it's a local user, all fields are allowed.

The avatar 

</template>
<template #request>

Before sending this request and if you want to change a user's avatar, you need to send it first through the REST API `/api/importfile`. See [uploading files](index#uploading-files) for more info.

Make Axel an admin and change his avatar :

```js[Request]
{
    login: 'axel',
    type: 0
    avatar: 'abdecfgh.png'
}
```

</template>
<template #success-response>

```js[Response]
{
  code: 200
  message: {
	code: 'USER_EDITED'    
  }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_EDIT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Unknown user">

```js[Response]
{
    code: 404,
    message: {
        code: 'USER_NOT_EXISTS'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,
    message: {
        code: 'USER_EDIT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Nickname already exists">

```js[Response]
{
    code: 409,
    message: {
        code: 'USER_EDIT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Password too short">

```js[Response]
{
    code: 400,
    message: {
        code: 'PASSWORD_TOO_SHORT'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Reset user password

<endpoint command="resetUserPassword" front="closed">
<template #description>
Reset a user's password.

Resetting a local user password's needs the security code.

Resetting a online user password will fail if user hasn't entered an email in their profile. Otherwise, a mail will be sent to the user with a link to reset their password.

</template>
<template #request>

```js[Request]
{
  username: 'axel',
  password: 'lol',
  securityCode: 123456
}
```

</template>
<template #success-response>
<base-code-block label="Local user">

```js[Response]
{
    code: 200,
    message: {
        code: 'USER_RESETPASSWORD_SUCCESS'
    }
}
```

</base-code-block>
<base-code-block label="Online user">

```js[Response]
{
    code: 200,
    message: {
        code: 'USER_RESETPASSWORD_ERROR'
    }
}
```

</base-code-block>
</template>

<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_RESETPASSWORD_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Wrong security code">

```js[Response]
{
    code: 403,
    message: {
        code: 'USER_RESETPASSWORD_WRONGSECURITYCODE'
    }
}
```

</base-code-block>
<base-code-block label="Error (online)">

```js[Response]
{
    code: 403,
    message: {
        code: 'USER_RESETPASSWORD_ONLINE_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Get User (my account)

<endpoint command="getMyAccount" front="closed" auth user="guest">
<template #description>
Get your own account information. 
</template>
<template #request>

Your authentification token is used to identify you and return your information.

</template>
<template #success-response>

See [Get User response](#get-user). All fields which return null will now return something, save for `password`.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_VIEW_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Delete user (my account)

<endpoint command="deleteMyUser" front="closed" auth user="user">
<template #description>
Delete your own account (locally).

This does not affect your online account.
</template>
<template #request>

Your authentification token is used to identify you and return your information.

</template>
<template #success-response>

```js[Response]
{
    code: 200,
    message: {
        code: 'USER_DELETED'
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_DELETE_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Edit user (my account)

<endpoint command="editMyAccount" front="closed" auth user="admin">
<template #description>
Edit your own profile.

This affects your online account if your user is an online one.

</template>
<template #request>

You'll need to provide an `onlineAuthorization` token to edit your online account.

See the [Edit User request](#edit-user) for more information.

</template>
<template #success-response>

If you're a online user, your online token will be displayed.

```js[Response]
{
    code: 200
    message: {
        code: 'USER_EDITED'
        data: {
            onlineAuthorization: <online token>
        }
    }
}
```

</template>
<template #errors>
<base-code-block label="User unknown">

```js[Response]
{
    code: 404
    message: {
        code: 'USER_NOT_EXISTS'
    }
}
```

</base-code-block>
<base-code-block label="Guests not allowed to edit">

```js[Response]
{
    code: 403
    message: {
        code: 'GUESTS_CANNOT_EDIT'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400
    message: {
        code: 'USER_EDIT_ERROR'
    }
}
```

</base-code-block>

<base-code-block label="User cannot change type">

```js[Response]
{
    code: 403
    message: {
        code: 'USER_CANNOT_CHANGE_TYPE'
    }
}
```

</base-code-block>
<base-code-block label="Nickname already exists">

```js[Response]
{
    code: 409
    message: {
        code: 'USER_EDIT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Password too short">

```js[Response]
{
    code: 400
    message: {
        code: 'PASSWORD_TOO_SHORT'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Convert my local user to online

<endpoint command="convertMyLocalUserToOnline" front="closed" auth user="user">
<template #description>
Convert my own local account to an online one
</template>
<template #request>

Your authentification token is used to identify you and return your information.

You still need to provide some information :

```js[Request]
{
  instance: 'mykmrepo.net' // create user on mykmrepo.net
  password: 'lol',     // your local password to confirm creation  
}
```

</template>
<template #success-response>

You get your `onlineAuthorization` token with the response :

```js[Response]
{
    code: 200,
    message: {
        code: 'USER_CONVERTED',
        data: {
            token: 'abcdefgh',
            onlineToken: 'abcdefghijkl'
        }
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_CONVERT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Admin error">

User admin cannot be converted.

```js[Response]
{
    code: 500,
    message: {
        code: 'ADMIN_CONVERT_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400    
}
```

</base-code-block>
</template>
</endpoint>

## Convert my online user to local

<endpoint command="convertMyOnlineUserToLocal" front="closed" auth user="user">
<template #description>
This is basically to delete your online account. This will thus remove your data from the server you're logged on, but keep your account local.

Use with care.

</template>
<template #request>

Your authentification token is used to identify you and return your information.

You still need to provide your password for confirmation

```js[Request]
{
  password: 'lol'
}
```

</template>
<template #success-response>

You get your new `authorization` token with the response :

```js[Response]
{
    code: 200,
    message: {
        code: 'USER_DELETED_ONLINE',
        data: {
            token: 'abcdefgh',            
        }
    }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'USER_DELETE_ERROR_ONLINE'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

```js[Response]
{
    code: 400,    
}
```

</base-code-block>
</template>
</endpoint>

## Refresh user's anime list

<endpoint command="refreshAnimeList" front="closed" auth user="user">
<template #description>
Triggers an anime list refresh. This asks the user's attached server to refresh the list from the selected anime list provider. Once done it'll get pushed to the user's profile on the local app.


</template>
<template #request>

The request uses the `authorization` and `onlineAuthorization` headers to work.

</template>
<template #success-response>

Simply resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'REFRESH_ANIME_LIST_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Get user's anime list's songs

<endpoint command="getAnimeList" front="closed" auth user="user">
<template #description>

Returns songs based on the user's anime list.

</template>
<template #request>

See [getKaras](karas#get-songs) for more information on this command's parameters. 

</template>
<template #success-response>

See [getKaras](karas#get-songs) for succesful responses.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'ANIME_LIST_VIEW_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Get user's anime list's songs

<endpoint command="getAnimeList" front="closed" auth user="user">
<template #description>

Returns songs based on the user's anime list.

</template>
<template #request>

See [getKaras](karas#get-songs) for more information on this command's parameters. 

</template>
<template #success-response>

See [getKaras](karas#get-songs) for succesful responses.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'ANIME_LIST_VIEW_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>
