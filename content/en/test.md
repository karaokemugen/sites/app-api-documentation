---
title: Test
description: Routes used only during test mode
position: 100
category: Endpoints
---

## Tests

These routes are only open when the app is run with the `--test` command line flag, and are only used by the unit tests.

## Get State

<endpoint command="getState">
<template #description>

Get complete application state.

</template>
<template #success-response>

See the `state.d.ts` definition file for what to expect from the state.

</template>
</endpoint>

## Get full config

<endpoint command="getFullConfig">
<template #description>
Returns the full configuration, without any edited/removed items
</template>
<template #success-response>

See `config.d.ts` definition file for what to expect.

</template>
</endpoint>