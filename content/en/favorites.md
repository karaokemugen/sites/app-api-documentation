---
title: Favorites
description: Manage user favorites
position: 5
category: Endpoints
---

## Get favorites

<endpoint command="getFavorites" front="restricted" auth user="guest">
<template #description>
Get a user's favorites. It uses your authentification token to find the right user's favorites.
</template>
<template #request>

```js[Request]
{
    filter: 'Dragon Ball', // Search words to filter the results with.
    from: 0, // Results to start from
    size: 400, // Number of results to return	
}
```

</template>
<template #success-response>

See [getKaras response](karas#get-songs)

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'FAVORITES_VIEW_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>

## Get favorites (micro)

<endpoint command="getFavoritesMicro" front="restricted" auth user="guest">
<template #description>
Get a user's favorites with minimal information. It uses your authentification token to find the right user's favorites.
</template>
<template #request>

```js[Request]
{
    from: 0, // Results to start from
    size: 400, // Number of results to return    
}
```

</template>
<template #success-response>

If user is a guest, it returns an empty array.

```js[Response]
[
	{kid: '2fed9947-a1b4-4a36-b8da-0e417d2b8ee6'},
	{kid: '86221fc3-2b71-4e9e-8fbb-4aa39923452a'},
	{kid: '39c1d4d6-26e0-422d-9e29-971eb3b015d6'}
]
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'FAVORITES_VIEW_ERROR'
    }
}
```

</base-code-block>

</template>
</endpoint>


## Add favorites

<endpoint command="addFavorites" front="open" auth user="admin">
<template #description>
Add favorites to your favorites list
</template>
<template #request>

```js[Request]
{
    kids: [
		'2fed9947-a1b4-4a36-b8da-0e417d2b8ee6',
		'86221fc3-2b71-4e9e-8fbb-4aa39923452a',
		'39c1d4d6-26e0-422d-9e29-971eb3b015d6'
	]
}
```

</template>
<template #success-response>

Call just resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'FAVORITES_ADDED_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Remove favorites

<endpoint command="deleteFavorites" front="open" auth user="admin">
<template #description>
Remove favorites from your list
</template>
<template #request>

```js[Request]
{
    kids: [
		'2fed9947-a1b4-4a36-b8da-0e417d2b8ee6',
		'86221fc3-2b71-4e9e-8fbb-4aa39923452a',
		'39c1d4d6-26e0-422d-9e29-971eb3b015d6'
	]
}
```

</template>
<template #success-response>

Call just resolves.

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'FAVORITES_DELETED_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Export favorites

<endpoint command="exportFavorites" front="restricted" auth user="user">
<template #description>
Export your favorites in a JSON format
</template>
<template #request>

No parameters needed, username will be decided from your `authorization` token

</template>
<template #success-response>

```js[Response]
{
  Header: { version: 1, description: 'Karaoke Mugen Favorites List File' },
  Favorites: [
    {
      kid: '54171ae9-4de8-48fe-bd0e-fc73fc68a9d6',
      title: 'Open your Mind',
      songorder: null,
      serie: 'Aa! Megami-sama! (2005)',
      songtype: 'OP',
      language: 'jpn'
    },
	...
  ]
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'FAVORITES_EXPORTED_ERROR'
    }
}
```

</base-code-block>
</template>
</endpoint>

## Import favorites

<endpoint command="importFavorites" front="restricted" auth user="user">
<template #description>
Import your favorites from a JSON data file
</template>
<template #request>

```js[Request]
{
  favorites: {
	Header: { version: 1, description: 'Karaoke Mugen Favorites List File' },
	Favorites: [
		{
		kid: '54171ae9-4de8-48fe-bd0e-fc73fc68a9d6',
		title: 'Open your Mind',
		songorder: null,
		serie: 'Aa! Megami-sama! (2005)',
		songtype: 'OP',
		language: 'jpn'
		},
		...
	]
  }
}
```

</template>
<template #success-response>

```js[Response]
{
  code: 200:
  message: {
      code: 'FAVORITES_IMPORTED'
  }
}
```

</template>
<template #errors>
<base-code-block label="Error">

```js[Response]
{
    code: 500,
    message: {
        code: 'FAVORITES_IMPORTED_ERROR'
    }
}
```

</base-code-block>
<base-code-block label="Bad request">

Sent when request data is not JSON.

```js[Response]
{
    code: 400
}
```

</base-code-block>
</template>
</endpoint>
